package com.pivotasoft.selltezhygiene;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pivotasoft.selltezhygiene.Adapters.IssuesListAdapter;
import com.pivotasoft.selltezhygiene.Apis.RetrofitClient;
import com.pivotasoft.selltezhygiene.Response.IssuesListResponse;
import com.pivotasoft.selltezhygiene.Singleton.AppController;
import com.pivotasoft.selltezhygiene.Storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssuesListActivity extends AppCompatActivity {


    String orderId;
    AppController appController;
    @BindView(R.id.recyclerIssues)
    RecyclerView recyclerIssues;
    @BindView(R.id.txtError)
    TextView txtError;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private PrefManager pref;
    String userId, tokenValue, deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issues_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Issues");

        appController = (AppController) getApplication();

        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");


        if (getIntent() != null) {
            orderId = getIntent().getStringExtra("Order_ID");
            Log.d("TAG", "onCreate: " + orderId);
        }

        if (appController.isConnection()) {

            getIssues();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;


        }

        return super.onOptionsItemSelected(item);
    }


    private void getIssues() {

        progressBar.setVisibility(View.VISIBLE);
        Call<IssuesListResponse> call = RetrofitClient.getInstance().getApi().issuesList(tokenValue, userId);
        call.enqueue(new Callback<IssuesListResponse>() {
            @Override
            public void onResponse(Call<IssuesListResponse> call, Response<IssuesListResponse> response) {
                if (response.isSuccessful()) {

                    progressBar.setVisibility(View.GONE);
                    IssuesListResponse myOrdersResponse = response.body();

                    List<IssuesListResponse.IssuelistdataBean> modulesBeanList = response.body().getIssuelistdata();

                    if (modulesBeanList.size() != 0) {
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        recyclerIssues.setLayoutManager(layoutManager);
                        recyclerIssues.setItemAnimator(new DefaultItemAnimator());

                        IssuesListAdapter myOrderAdapter = new IssuesListAdapter(IssuesListActivity.this, modulesBeanList);
                        recyclerIssues.setAdapter(myOrderAdapter);
                        myOrderAdapter.notifyDataSetChanged();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toasty.normal(IssuesListActivity.this, myOrdersResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText("No Issues Found !");
                    }

                } else {
                    progressBar.setVisibility(View.GONE);
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("No Issues Found !");
                }
            }

            @Override
            public void onFailure(Call<IssuesListResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toasty.normal(IssuesListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}

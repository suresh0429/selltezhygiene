package com.pivotasoft.selltezhygiene;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pivotasoft.selltezhygiene.Adapters.CartAdapter;
import com.pivotasoft.selltezhygiene.Apis.RetrofitClient;
import com.pivotasoft.selltezhygiene.Interface.CartProductClickListener;
import com.pivotasoft.selltezhygiene.Model.Product;
import com.pivotasoft.selltezhygiene.Response.CartResponse;
import com.pivotasoft.selltezhygiene.Singleton.AppController;
import com.pivotasoft.selltezhygiene.Storage.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity implements CartProductClickListener {

    @BindView(R.id.productsRcycler)
    RecyclerView productsRcycler;
    @BindView(R.id.txtSubTotal)
    TextView txtSubTotal;
    @BindView(R.id.btnCheckOut)
    Button btnCheckOut;
    @BindView(R.id.bottamlayout)
    LinearLayout bottamlayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;


    ArrayList<Product> productArrayList;
    CartProductClickListener cartProductClickListener;
    CartAdapter cartAdapter;
    AppController appController;
    @BindView(R.id.txtAlert)
    TextView txtAlert;
    private PrefManager pref;

    String userId, tokenValue, deviceId,storeId,storeName,storeFcmKey;
    //int totalQty = 0;
    int totalQuantity = 0;
    int minimumOrder = 500;

    public String length;
    public String module;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cart");

        cartProductClickListener = (CartProductClickListener) this;

        appController = (AppController) getApplication();

        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        if (getIntent() != null) {
            storeId = getIntent().getStringExtra("storeId");
            storeName = getIntent().getStringExtra("storeName");
            storeFcmKey = getIntent().getStringExtra("storeFcmKey");
        }

        if (appController.isConnection()) {

            // Showing progress dialog before making http request

            progressBar.setVisibility(View.VISIBLE);

            // cart count Update
            appController.cartCount(userId, tokenValue);

            cartData();


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("storeName", storeName);
                    intent.putExtra("storeFcmKey", storeFcmKey);
                    intent.putExtra("storeId", storeId);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

            // app.internetDilogue(KitchenitemListActivity.this);

        }


    }


    @Override
    public void onMinusClick(Product cartItemsBean) {
        int i = productArrayList.indexOf(cartItemsBean);

        if (cartItemsBean.getQuantity() > 1) {

            int quantity = cartItemsBean.getQuantity() - 1;
            int itemTotal = cartItemsBean.getFinalprice() * quantity;
            Log.d("TAG", "onMinusClick: "+quantity);

            Product updatedProduct = new Product(cartItemsBean.getCartid(), cartItemsBean.getBuyerid(), cartItemsBean.getStoreid(),
                    cartItemsBean.getProductid(), cartItemsBean.getFinalprice(), quantity, itemTotal,
                    cartItemsBean.getAddedat(), cartItemsBean.getTitle(), cartItemsBean.getMeasureunits(), cartItemsBean.getProductpic());

            productArrayList.remove(cartItemsBean);
            productArrayList.add(i, updatedProduct);

            updateQuantity(updatedProduct);
            cartAdapter.notifyDataSetChanged();


            calculateCartTotal();

        }
    }

    @Override
    public void onPlusClick(Product cartItemsBean) {
        int i = productArrayList.indexOf(cartItemsBean);

        int quantity = cartItemsBean.getQuantity() + 1;
        int itemTotal = cartItemsBean.getFinalprice() * quantity;
        Log.d("TAG", "onMinusClick: "+quantity);


        Product updatedProduct = new Product(cartItemsBean.getCartid(), cartItemsBean.getBuyerid(), cartItemsBean.getStoreid(),
                cartItemsBean.getProductid(), cartItemsBean.getFinalprice(), quantity, itemTotal,
                cartItemsBean.getAddedat(), cartItemsBean.getTitle(), cartItemsBean.getMeasureunits(), cartItemsBean.getProductpic());

        productArrayList.remove(cartItemsBean);
        productArrayList.add(i, updatedProduct);

        Log.e("QUNATITY", "" + updatedProduct.getQuantity());
        updateQuantity(updatedProduct);

        cartAdapter.notifyDataSetChanged();


        calculateCartTotal();
    }

    @Override
    public void onSaveClick(Product product) {

    }

    @Override
    public void onRemoveDialog(final Product product) {
        final Dialog dialog = new Dialog(CartActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_dialog);


        // set the custom dialog components - text, image and button
        TextView te = (TextView) dialog.findViewById(R.id.txtAlert);
        te.setText("Are you want to Delete?");

        TextView yes = (TextView) dialog.findViewById(R.id.btnYes);
        TextView no = (TextView) dialog.findViewById(R.id.btnNo);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete(product);
                cartAdapter.notifyDataSetChanged();
                dialog.dismiss();
                Snackbar.make(parentLayout, "Deleted Successfully", Snackbar.LENGTH_SHORT).show();


            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onWishListDialog(final Product product) {

        final Dialog dialog = new Dialog(CartActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_dialog);


        // set the custom dialog components - text, image and button
        TextView te = (TextView) dialog.findViewById(R.id.txtAlert);
        te.setText("Add to WishList?");

        TextView yes = (TextView) dialog.findViewById(R.id.btnYes);
        TextView no = (TextView) dialog.findViewById(R.id.btnNo);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //saveWishList(product);
                cartAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    // Remove from Cart
    public void delete(final Product product) {
        // Showing progress dialog before making http request
        progressBar.setVisibility(View.VISIBLE);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().removeCart(tokenValue,product.getCartid());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                progressBar.setVisibility(View.GONE);
                ResponseBody wishListPostResponse = response.body();

                if (response.isSuccessful()) {

                    int i = productArrayList.indexOf(product);
                    if (i == -1) {
                        throw new IndexOutOfBoundsException();
                    }
                    productArrayList.remove(productArrayList.get(i));

                    // cart count Update
                    appController.cartCount(userId, tokenValue);
                    invalidateOptionsMenu();

                    calculateCartTotal();
                    cartAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                progressBar.setVisibility(View.GONE);
            }
        });
    }



    private void cartData() {

        productArrayList = new ArrayList<>();

        Call<CartResponse> call = RetrofitClient.getInstance().getApi().getCart(tokenValue, userId);
        call.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {

                progressBar.setVisibility(View.GONE);

                final CartResponse cartResponse = response.body();

                if (response.isSuccessful()) {

                    for (CartResponse.CartlistdataBean cartItemsBean : cartResponse.getCartlistdata()) {


                        productArrayList.add(new Product(cartItemsBean.getCartid(), cartItemsBean.getBuyerid(), cartItemsBean.getStoreid(),
                                cartItemsBean.getProductid(), Integer.parseInt(cartItemsBean.getFinalprice()), Integer.parseInt(cartItemsBean.getQuantity()), Integer.parseInt(cartItemsBean.getItemtotal()),
                                cartItemsBean.getAddedat(), cartItemsBean.getTitle(), cartItemsBean.getMeasureunits(), cartItemsBean.getProductpic()));


                        Log.e("LENGTH", "" + productArrayList.size());

                        length = String.valueOf(productArrayList.size());


                    }

                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    productsRcycler.setLayoutManager(mLayoutManager1);
                    productsRcycler.setItemAnimator(new DefaultItemAnimator());
                    productsRcycler.setHasFixedSize(true);

                    cartAdapter = new CartAdapter(CartActivity.this, productArrayList, cartProductClickListener);
                    productsRcycler.setAdapter(cartAdapter);
                    cartAdapter.notifyDataSetChanged();
                    // calculate total amount
                    calculateCartTotal();
                }
                else {
                    setContentView(R.layout.emptycart);
                    Button btnGotohome = (Button) findViewById(R.id.btnGotohome);
                    btnGotohome.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(CartActivity.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("storeId",storeId);
                            intent.putExtra("storeName",storeName);
                            intent.putExtra("storeFcmKey",storeFcmKey);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }
                    });
                  //  Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + "No Cart Items Found"+ "</font>"), Snackbar.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {

                progressBar.setVisibility(View.GONE);
                Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();

            }
        });


    }

    // update quantity
    public void updateQuantity(final Product product) {

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateCart(tokenValue,product.getCartid(), String.valueOf(product.getFinalprice()), String.valueOf(product.getQuantity()),String.valueOf(product.getItemtotal()),product.getAddedat());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    ResponseBody baseResponse = response.body();

                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    // total Amount
    public void calculateCartTotal() {

        int grandTotal = 0;



        for (Product order : productArrayList) {

            grandTotal += (ParseDouble(String.valueOf(order.getFinalprice())) * order.getQuantity());

            storeId = order.getStoreid();

        }

        Log.e("TOTAL", "" + productArrayList.size() + "Items | " + "  DISCOUNT : " + grandTotal);

        txtSubTotal.setText(productArrayList.size() + " Items | Rs " + grandTotal);

        if (grandTotal >= minimumOrder) {

            btnCheckOut.setVisibility(View.VISIBLE);
            txtAlert.setVisibility(View.GONE);
        } else {

            btnCheckOut.setVisibility(View.INVISIBLE);
            txtAlert.setVisibility(View.VISIBLE);
            txtAlert.setText("Minimum Order Must Be Greater Than Rs."+minimumOrder+"*");

        }

        final int finalGrandTotal = grandTotal;
        btnCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CartActivity.this, AddressListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Checkout",true);
                intent.putExtra("grandTotal", finalGrandTotal);
                intent.putExtra("storeId",storeId);
                intent.putExtra("storeName",storeName);
                intent.putExtra("storeFcmKey",storeFcmKey);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        if (productArrayList.size() == 0) {

            setContentView(R.layout.emptycart);
            Button btnGotohome = (Button) findViewById(R.id.btnGotohome);
            btnGotohome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(CartActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("storeId",storeId);
                    intent.putExtra("storeName",storeName);
                    intent.putExtra("storeFcmKey",storeFcmKey);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

    }


    private double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch (Exception e) {
                return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        } else return 0;
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        // cart count Update
        appController.cartCount(userId, tokenValue);

        cartData();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // cart count Update
        appController.cartCount(userId, tokenValue);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                invalidateOptionsMenu();
                onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


}

package com.pivotasoft.selltezhygiene;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.pivotasoft.selltezhygiene.Adapters.ProductAadpter;
import com.pivotasoft.selltezhygiene.Apis.RetrofitClient;
import com.pivotasoft.selltezhygiene.Helper.Converter;

import com.pivotasoft.selltezhygiene.Response.ProductResponse;
import com.pivotasoft.selltezhygiene.Storage.PrefManager;


import com.pivotasoft.selltezhygiene.Singleton.AppController;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;


public class ProductListActivity extends AppCompatActivity {
    SwipeRefreshLayout swipeRefreshLayout;
    AppController appController;
    @BindView(R.id.filterLayout)
    CardView filterLayout;
    @BindView(R.id.txtError)
    TextView txtError;
    private ProductAadpter adapter;
    private RecyclerView rcProduct;
    LinearLayout sortLinear, filterLinear;
    private PrefManager pref;
    String userid, title, module,catId,subcatId,tokenValue,deviceId,storeId,storeName,storeFcmKey;


    int cartindex;
    String positionValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userid = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        if (getIntent().getExtras() != null) {
            catId = getIntent().getStringExtra("catId");
            subcatId = getIntent().getStringExtra("subcatId");
            title = getIntent().getStringExtra("title");
            storeId = getIntent().getStringExtra("storeId");
            storeName = getIntent().getStringExtra("storeName");
            storeFcmKey = getIntent().getStringExtra("storeFcmKey");
        }

        getSupportActionBar().setTitle(title);
        appController = (AppController) getApplicationContext();


        // init SwipeRefreshLayout and ListView
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.simpleSwipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorBlue, R.color.colorPrimary);

        rcProduct = (RecyclerView) findViewById(R.id.rcProduct);


        // cart count
        appController.cartCount(userid,tokenValue);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindex", "" + cartindex);
        invalidateOptionsMenu();


        if (appController.isConnection()) {

            prepareProductData(subcatId);

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    prepareProductData(subcatId);

                    swipeRefreshLayout.setRefreshing(false);


                }
            });


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

            // app.internetDilogue(KitchenitemListActivity.this);

        }

    }

    private void prepareProductData(final String subcatId) {

        swipeRefreshLayout.setRefreshing(true);

        Call<ProductResponse> call = RetrofitClient.getInstance().getApi().products(storeId,subcatId,tokenValue);

        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, retrofit2.Response<ProductResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                ProductResponse productResponse = response.body();

                if (response.isSuccessful()) {


                    List<ProductResponse.ProductsdataBean> filteredProductsBeanList = response.body() != null ? response.body().getProductsdata() : null;

                    if ((filteredProductsBeanList != null ? filteredProductsBeanList.size() : 0) !=0){

                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
                        rcProduct.setLayoutManager(mLayoutManager);
                        rcProduct.setItemAnimator(new DefaultItemAnimator());

                        adapter = new ProductAadpter(getApplicationContext(), filteredProductsBeanList,storeName,storeFcmKey);
                        rcProduct.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }else {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(productResponse.getMessage());
                    }





                } else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Snackbar.make(rcProduct, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Snackbar.make(rcProduct, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                            break;
                        default:
                            Snackbar.make(rcProduct, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                Snackbar.make(rcProduct, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();

            }
        });


    }

    @Override
    protected void onRestart() {

        appController.cartCount(userid,tokenValue);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onRestart();
    }

    @Override
    protected void onStart() {
        appController.cartCount(userid,tokenValue);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onStart();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_cart);
        menuItem.setIcon(Converter.convertLayoutToImage(ProductListActivity.this, cartindex, R.drawable.ic_actionbar_bag));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_cart:
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("storeId",storeId);
                intent.putExtra("storeName",storeName);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.action_search:
                Intent intent1 = new Intent(ProductListActivity.this, SearchActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

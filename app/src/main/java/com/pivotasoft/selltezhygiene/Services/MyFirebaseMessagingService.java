package com.pivotasoft.selltezhygiene.Services;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;

import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.pivotasoft.selltezhygiene.OrderIdActvity;
import com.pivotasoft.selltezhygiene.R;
import com.pivotasoft.selltezhygiene.SplashActivity;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

public class MyFirebaseMessagingService extends  FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        Log.d(TAG, "onMessageReceived: "+remoteMessage.getData().get("image"));
        //handle when receive notification via data event
        if(remoteMessage.getData().size()>0){

            //Bitmap bitmap = getBitmapfromUrl(remoteMessage.getData().get("image"));
            Bitmap bitmap = null;
            // showNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("content"), bitmap);
            bigTextStyleNotification(remoteMessage.getData().get("title"),remoteMessage.getData().get("content"));
            //  showNotification1(remoteMessage.getData().get("title"), remoteMessage.getData().get("content"), bitmap);

        }

        //handle when receive notification
        if(remoteMessage.getNotification()!=null){
            bigTextStyleNotification(remoteMessage.getData().get("title"),remoteMessage.getData().get("content"));
            //showNotification(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(), null);
        }

    }

    private RemoteViews getCustomDesign(String title, String message){
        RemoteViews remoteViews=new RemoteViews(getApplicationContext().getPackageName(), R.layout.notification);
        remoteViews.setTextViewText(R.id.title,title);
        remoteViews.setTextViewText(R.id.message,message);
        remoteViews.setImageViewResource(R.id.icon,R.drawable.selltez_customer_splash);

        return remoteViews;
    }

    public void showNotification(String title, String message, String orderId, Bitmap bitmap){
        Intent intent=new Intent(this, OrderIdActvity.class);
        String channel_id="selltez_channel";
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("orderId", orderId);
        PendingIntent pendingIntent= PendingIntent.getActivity(this,0,intent, PendingIntent.FLAG_ONE_SHOT);
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder=new NotificationCompat.Builder(getApplicationContext(),channel_id)
                //.setSmallIcon(R.drawable.ic_app_icon)
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap))/*Notification with Image*/

                .setSound(uri)
                .setAutoCancel(true)
                .setVibrate(new long[]{1000,1000,1000,1000,1000})
                .setOnlyAlertOnce(true)
                .setContentIntent(pendingIntent);

        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.JELLY_BEAN){
            builder.setSmallIcon(R.drawable.ic_shopping_cart);
            //builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            builder=builder.setContent(getCustomDesign(title,message));
        }
        else{
            builder.setSmallIcon(R.drawable.selltez_customer_splash);
            builder=builder.setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.selltez_customer_splash);
        }

        NotificationManager notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel=new NotificationChannel(channel_id,"web_app", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setSound(uri,null);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notificationManager.notify(0,builder.build());

    }

    public void showNotification1(String title, String message, String orderId, Bitmap bitmap){
        Intent intent=new Intent(this, OrderIdActvity.class);
        String channel_id="selltez_channel";
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("orderId", orderId);
        PendingIntent pendingIntent= PendingIntent.getActivity(this,0,intent, PendingIntent.FLAG_ONE_SHOT);
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder=new NotificationCompat.Builder(getApplicationContext(),channel_id)
                .setSmallIcon(R.drawable.selltez_customer_splash)
                .setSound(uri)
                .setAutoCancel(true)
                .setVibrate(new long[]{1000,1000,1000,1000,1000})
                .setOnlyAlertOnce(true)
                .setContentIntent(pendingIntent);

        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.JELLY_BEAN){
            builder.setSmallIcon(R.drawable.ic_shopping_cart);
          //  builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            builder=builder.setContent(getCustomDesign(title,message));
        }
        else{
            builder.setSmallIcon(R.drawable.selltez_customer_splash);
            builder=builder.setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.selltez_customer_splash);
        }

        NotificationManager notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel=new NotificationChannel(channel_id,"web_app", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setSound(uri,null);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notificationManager.notify(0,builder.build());


    }

    //app part ready now let see how to send differnet users
    //like send to specific device
    //like specifi topic


    /*
     *To get a Bitmap image from the URL received
     * */
    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }

    private void bigTextStyleNotification(String title , String message) {
        int NOTIFICATION_ID = 1;
        Intent intent=new Intent(this, SplashActivity.class);
        String channel_id="selltez_channel";
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent= PendingIntent.getActivity(this,0,intent, PendingIntent.FLAG_ONE_SHOT);
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,channel_id);
        builder.setSmallIcon(R.drawable.ic_shopping_cart);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.selltez_customer_splash));
        builder.setContentTitle(title);
        // builder.setSubText(message);
        builder.setContentText(message);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        builder.setSound(uri);
        // builder.setVibrate(new long[]{1000,1000,1000,1000,1000});
        // builder.setOnlyAlertOnce(true);
        builder.setAutoCancel(true);
        builder.setContentIntent(pendingIntent);
        // builder.addAction(android.R.drawable.ic_delete, "DISMISS", dismissIntent);
        // builder.addAction(android.R.drawable.ic_menu_send, "OPEN APP", launchIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            if (notificationManager != null && notificationManager.getNotificationChannel(channel_id) == null) {
                NotificationChannel notificationChannel = new NotificationChannel(channel_id, "web_app", NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.setSound(uri, null);
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
        // Will display the notification in the notification bar
        if (notificationManager != null) {
            notificationManager.notify(NOTIFICATION_ID, builder.build());
        }
    }


}

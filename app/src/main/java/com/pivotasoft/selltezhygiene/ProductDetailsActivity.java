package com.pivotasoft.selltezhygiene;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pivotasoft.selltezhygiene.Adapters.ViewPagerAdapter;
import com.pivotasoft.selltezhygiene.Apis.RetrofitClient;
import com.pivotasoft.selltezhygiene.Helper.Converter;
import com.pivotasoft.selltezhygiene.Singleton.AppController;
import com.pivotasoft.selltezhygiene.Storage.PrefManager;
import com.rd.PageIndicatorView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.pivotasoft.selltezhygiene.Storage.Utilities.capitalize;

public class ProductDetailsActivity extends AppCompatActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.pageIndicatorView)
    PageIndicatorView pageIndicatorView;
    @BindView(R.id.txtProductName)
    TextView txtProductName;
    @BindView(R.id.txtPrice)
    TextView txtPrice;
    @BindView(R.id.txtDisPrice)
    TextView txtDisPrice;
    @BindView(R.id.txtDescription)
    TextView txtDescription;
    @BindView(R.id.btnGotoCart)
    Button btnGotoCart;
    @BindView(R.id.btnAddtocart)
    Button btnAddtocart;
    @BindView(R.id.bootamLayout)
    CardView bootamLayout;
    @BindView(R.id.progressBarMain)
    ProgressBar progressBarMain;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;

    AppController appController;
    List<String> viewPagerItemslist = new ArrayList<>();
    @BindView(R.id.product_minus)
    TextView productMinus;
    @BindView(R.id.product_quantity)
    TextView productQuantity;
    @BindView(R.id.product_plus)
    TextView productPlus;
    @BindView(R.id.txtWeight)
    TextView txtWeight;

    private PrefManager pref;
    String userid;
    int cartindex;
    int count = 1;

    String id, title,discount,price,discription,status,productImage,weight, storeId,deviceId, tokenValue,currentDateandTime,storeName,storeFcmKey;
    String productIdsSet;
    String cartTypeSet;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());
        Log.d("TAG", "onClick: "+currentDateandTime);

        loadOncreateData();


    }

    private void loadOncreateData() {
        if (getIntent().getExtras() != null) {
            id = getIntent().getStringExtra("productId");
            title = getIntent().getStringExtra("productName");
            price = getIntent().getStringExtra("price");
            discount = getIntent().getStringExtra("discount");
            discription = getIntent().getStringExtra("discription");
            status = getIntent().getStringExtra("status");
            weight = getIntent().getStringExtra("weight");
            storeId = getIntent().getStringExtra("storeId");
            storeName = getIntent().getStringExtra("storeName");
            storeFcmKey = getIntent().getStringExtra("storeFcmKey");
            productImage = getIntent().getStringExtra("productImage");
            Log.d("TAG", "loadOncreateData: "+productImage);
        }
        getSupportActionBar().setTitle(capitalize(title));

        appController = (AppController) getApplicationContext();

        pref = new PrefManager(getApplicationContext());

        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userid = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        if (appController.isConnection()) {

          //  progressBarMain.setVisibility(View.VISIBLE);

            //prepareProductDetailsData(id, deviceId);
            productsDetails();

            // cart count
            appController.cartCount(userid, tokenValue);
            SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
            cartindex = preferences.getInt("itemCount", 0);
            productIdsSet = preferences.getString("productIds", null);
            cartTypeSet = preferences.getString("cartType", null);


            Log.e("productIdsSet", "" + productIdsSet + "------" + cartTypeSet);


            invalidateOptionsMenu();


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

            // app.internetDilogue(KitchenitemListActivity.this);

        }


    }

    private void productsDetails(){
        txtProductName.setText(capitalize(title));

        txtPrice.setText(getResources().getString(R.string.Rs) + discount);
        txtWeight.setText( weight);

        txtDisPrice.setText(getResources().getString(R.string.Rs) + price );
        txtDisPrice.setPaintFlags(txtDisPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        txtDisPrice.setTextColor(Color.RED);
        txtDescription.setText(discription);

        // check avalibulity
        if (status.equalsIgnoreCase("2")){
            btnAddtocart.setClickable(false);
            btnAddtocart.setEnabled(false);
            btnAddtocart.setBackgroundColor(Color.parseColor("#C0C0C0"));
        }


        // slider Images
        List<String> namesList = new ArrayList<>();
        namesList.add(productImage);

        Log.e("ALLIMAGESLIDES", "" + namesList.size());


        viewPagerItemslist.clear();
        for (String name : namesList) {

            System.out.println(name);

            Log.e("IMAGESLIDES", "" + name);

            viewPagerItemslist.add( name);
        }

        final PageIndicatorView pageIndicatorView = findViewById(R.id.pageIndicatorView);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getApplicationContext(), viewPagerItemslist,status);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pageIndicatorView.setSelection(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        // quantity decrease
        productMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (count > 1) {
                    count--;
                    productQuantity.setText("" + count);
                }
            }
        });

        // quantity increase
        productPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                productQuantity.setText("" + count);
            }
        });


        btnAddtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int itemTotal = Integer.parseInt(productQuantity.getText().toString()) * Integer.parseInt(discount);
                Log.d("TAG", "onClick: "+itemTotal);

                addtocartData(id, productQuantity.getText().toString(),itemTotal);

            }
        });

        btnGotoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("storeId",storeId);
                intent.putExtra("storeName",storeName);
                intent.putExtra("storeFcmKey",storeFcmKey);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }




    // submit details
    private void addtocartData(final String productid, String quantity, int itemTotal) {

        progressBarMain.setVisibility(View.VISIBLE);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().addCart(tokenValue,userid,storeId,productid,discount,quantity, String.valueOf(itemTotal),currentDateandTime);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBarMain.setVisibility(View.GONE);

                ResponseBody cartPostResponse = response.body();

                if (response.isSuccessful()) {

                    // Toast.makeText(ProductDetailsActivity.this, cartPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.WHITE + "\">" + "product added to cart" + "</font>"), Snackbar.LENGTH_SHORT).show();

                    // cart count updation
                    cartindex = cartindex + 1;
                    Log.e("CARTPLUSCOUNT", "" + cartindex);
                    SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt("itemCount", cartindex);
                    editor.apply();


                    // cart count Update
                    appController.cartCount(userid, tokenValue);
                    invalidateOptionsMenu();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarMain.setVisibility(View.GONE);
            }
        });
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        loadOncreateData();
        appController.cartCount(userid, tokenValue);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
    }

    @Override
    protected void onStart() {
        super.onStart();

        //  loadOncreateData();
        appController.cartCount(userid, tokenValue);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_cart);
        menuItem.setIcon(Converter.convertLayoutToImage(ProductDetailsActivity.this, cartindex, R.drawable.ic_actionbar_bag));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_cart:
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("storeId",storeId);
                intent.putExtra("storeName",storeName);
                intent.putExtra("storeFcmKey",storeFcmKey);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.action_search:
                Intent intent1 = new Intent(ProductDetailsActivity.this, SearchActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}

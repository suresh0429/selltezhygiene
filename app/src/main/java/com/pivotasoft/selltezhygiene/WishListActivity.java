package com.pivotasoft.selltezhygiene;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.google.gson.Gson;
import com.pivotasoft.selltezhygiene.Adapters.WishListAdapter;
import com.pivotasoft.selltezhygiene.Apis.RetrofitClient;
import com.pivotasoft.selltezhygiene.Storage.PrefManager;
import com.pivotasoft.selltezhygiene.Response.WishlistResponse;
import com.pivotasoft.selltezhygiene.Singleton.AppController;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

public class WishListActivity extends AppCompatActivity {
    AppController appController;
    @BindView(R.id.recyclerWishlist)
    RecyclerView recyclerWishlist;
    @BindView(R.id.txtError)
    TextView txtError;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private WishListAdapter wishListAdapter;
    private PrefManager pref;
    WishlistResponse wishlistResponse = new WishlistResponse();
    Gson gson;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Wishlist");

        appController = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");


        if (appController.isConnection()) {

            wishListData();


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }


    }

    private void wishListData() {

        progressBar.setVisibility(View.VISIBLE);

        Call<WishlistResponse> call = RetrofitClient.getInstance().getApi().getWhishlisList(userId);

        call.enqueue(new Callback<WishlistResponse>() {
            @Override
            public void onResponse(Call<WishlistResponse> call, retrofit2.Response<WishlistResponse> response) {
                progressBar.setVisibility(View.GONE);
                WishlistResponse wishlistResponse = response.body();

                if (response.isSuccessful()) {

                    progressBar.setVisibility(View.GONE);

                    if ((wishlistResponse != null ? wishlistResponse.getStatus() : 0) == 1) {


                            // wishlist Adapter
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            recyclerWishlist.setLayoutManager(mLayoutManager);
                            recyclerWishlist.setItemAnimator(new DefaultItemAnimator());

                            wishListAdapter = new WishListAdapter(WishListActivity.this, wishlistResponse.getWishList());
                            recyclerWishlist.setAdapter(wishListAdapter);
                            wishListAdapter.notifyDataSetChanged();


                    }  else {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(wishlistResponse.getMessage());
                    }


                }else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Snackbar.make(recyclerWishlist, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Snackbar.make(recyclerWishlist, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                            break;
                        default:
                            Snackbar.make(recyclerWishlist, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<WishlistResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Snackbar.make(recyclerWishlist, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();

            }
        });


    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;


        }
        return super.onOptionsItemSelected(item);
    }

}
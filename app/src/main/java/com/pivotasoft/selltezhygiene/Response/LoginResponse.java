package com.pivotasoft.selltezhygiene.Response;

import java.util.List;

public class LoginResponse {


    /**
     * userdata : [{"buyerid":"1","fullname":"Eeswar T","mobile":"9160106499","email":"gangadhar.t9@gmail.com","password":"cc03e747a6afbbcbf8be7668acfebee5","status":"2","userkey":"oYyROWQzc2JxupBqAZLVCwmin3egKPf1"}]
     * message : success
     */

    private String message;
    private List<UserdataBean> userdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserdataBean> getUserdata() {
        return userdata;
    }

    public void setUserdata(List<UserdataBean> userdata) {
        this.userdata = userdata;
    }

    public static class UserdataBean {
        /**
         * buyerid : 1
         * fullname : Eeswar T
         * mobile : 9160106499
         * email : gangadhar.t9@gmail.com
         * password : cc03e747a6afbbcbf8be7668acfebee5
         * status : 2
         * userkey : oYyROWQzc2JxupBqAZLVCwmin3egKPf1
         */

        private String buyerid;
        private String fullname;
        private String mobile;
        private String email;
        private String password;
        private String status;
        private String userkey;

        public String getBuyerid() {
            return buyerid;
        }

        public void setBuyerid(String buyerid) {
            this.buyerid = buyerid;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUserkey() {
            return userkey;
        }

        public void setUserkey(String userkey) {
            this.userkey = userkey;
        }
    }
}


    /**
     * status : 10100
     * message : You have been logged in successfully.
     * data : {"jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Njk1NjEwODIsImp0aSI6InRSK1E5eGRtVGljMzd2Rlp1XC9KcmhUT0VWTUs4b2JPRTNsZW52ZlRyb0FJPSIsImlzcyI6Imh0dHA6XC9cL3d3dy5raWxvbWFydC5pblwvcGhwLWpzb25cLyIsIm5iZiI6MTU2OTU2MTA4MywiZXhwIjoxNjAxMDk3MDgzLCJkYXRhIjp7InVzZXJfaWQiOiI1NSJ9fQ.mVpPpObzS-QZsaBZ4YXd7R4Dgsd6WHFMCffGxvxA90LhmytVAjLOmg3-xwemdH4q9b9tkytztLsNqrDYuQlW3g","user_id":"55","user_name":"Bhavani","email":"bhavani@innasoft.in","mobile":"8074294327","gender":"","mobile_verify_status":"0"}
     *//*

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        *//**
         * jwt : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Njk1NjEwODIsImp0aSI6InRSK1E5eGRtVGljMzd2Rlp1XC9KcmhUT0VWTUs4b2JPRTNsZW52ZlRyb0FJPSIsImlzcyI6Imh0dHA6XC9cL3d3dy5raWxvbWFydC5pblwvcGhwLWpzb25cLyIsIm5iZiI6MTU2OTU2MTA4MywiZXhwIjoxNjAxMDk3MDgzLCJkYXRhIjp7InVzZXJfaWQiOiI1NSJ9fQ.mVpPpObzS-QZsaBZ4YXd7R4Dgsd6WHFMCffGxvxA90LhmytVAjLOmg3-xwemdH4q9b9tkytztLsNqrDYuQlW3g
         * user_id : 55
         * user_name : Bhavani
         * email : bhavani@innasoft.in
         * mobile : 8074294327
         * gender :
         * mobile_verify_status : 0
         *//*

        private String jwt;
        private String user_id;
        private String user_name;
        private String email;
        private String mobile;
        private String gender;
        private String mobile_verify_status;

        public String getJwt() {
            return jwt;
        }

        public void setJwt(String jwt) {
            this.jwt = jwt;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getMobile_verify_status() {
            return mobile_verify_status;
        }

        public void setMobile_verify_status(String mobile_verify_status) {
            this.mobile_verify_status = mobile_verify_status;
        }
    }
}
*/
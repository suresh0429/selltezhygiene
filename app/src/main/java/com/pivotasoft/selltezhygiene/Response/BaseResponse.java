package com.pivotasoft.selltezhygiene.Response;

public class BaseResponse {


    /**
     * status : 10100
     * message : OTP has been send to your mobile.
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package com.pivotasoft.selltezhygiene.Response;

import java.util.List;

public class OrdersResponse {


    /**
     * ordersdata : [{"orderid":"38","storeid":"1","bookingdatetime":"2020-05-12 17:24:11","finalbill":"672","invoiceno":"ST000200000000038","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-13 11:00:00","title":"Swathi Market","mobile":"8919480920","fcmtoken":"ePYfPrzXWiY:APA91bFdKnAg7IIBUwIC8sMyk2cMWWx_Y9X4dNgpWJ8yQWWGPA1MzYRC7YJlwKvMqvYvAKZtGE4e8jTC5bjw1g3G5l9v8b7HnuA4-fZxbFJzY8i8SdO8MP0yccg75bAksrV37xUUrOQW"},{"orderid":"39","storeid":"1","bookingdatetime":"2020-05-12 18:24:28","finalbill":"631.2","invoiceno":"ST000200000000039","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-13 11:00:00","title":"Swathi Market","mobile":"8919480920","fcmtoken":"ePYfPrzXWiY:APA91bFdKnAg7IIBUwIC8sMyk2cMWWx_Y9X4dNgpWJ8yQWWGPA1MzYRC7YJlwKvMqvYvAKZtGE4e8jTC5bjw1g3G5l9v8b7HnuA4-fZxbFJzY8i8SdO8MP0yccg75bAksrV37xUUrOQW"},{"orderid":"40","storeid":"1","bookingdatetime":"2020-05-12 18:27:54","finalbill":"788.28","invoiceno":"ST000200000000040","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-13 11:00:00","title":"Swathi Market","mobile":"8919480920","fcmtoken":"ePYfPrzXWiY:APA91bFdKnAg7IIBUwIC8sMyk2cMWWx_Y9X4dNgpWJ8yQWWGPA1MzYRC7YJlwKvMqvYvAKZtGE4e8jTC5bjw1g3G5l9v8b7HnuA4-fZxbFJzY8i8SdO8MP0yccg75bAksrV37xUUrOQW"},{"orderid":"41","storeid":"1","bookingdatetime":"2020-05-12 18:52:58","finalbill":"590.4","invoiceno":"ST000200000000041","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-13 11:00:00","title":"Swathi Market","mobile":"8919480920","fcmtoken":"ePYfPrzXWiY:APA91bFdKnAg7IIBUwIC8sMyk2cMWWx_Y9X4dNgpWJ8yQWWGPA1MzYRC7YJlwKvMqvYvAKZtGE4e8jTC5bjw1g3G5l9v8b7HnuA4-fZxbFJzY8i8SdO8MP0yccg75bAksrV37xUUrOQW"},{"orderid":"48","storeid":"1","bookingdatetime":"2020-05-13 21:38:22","finalbill":"570","invoiceno":"ST000200000000048","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-14 11:00:00","title":"Swathi Market","mobile":"8919480920","fcmtoken":"ePYfPrzXWiY:APA91bFdKnAg7IIBUwIC8sMyk2cMWWx_Y9X4dNgpWJ8yQWWGPA1MzYRC7YJlwKvMqvYvAKZtGE4e8jTC5bjw1g3G5l9v8b7HnuA4-fZxbFJzY8i8SdO8MP0yccg75bAksrV37xUUrOQW"},{"orderid":"51","storeid":"1","bookingdatetime":"2020-05-13 21:51:37","finalbill":"570","invoiceno":"ST000200000000051","paymentmode":"online","paymentsummary":"pay_EpuD4E62mBVnLl","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-14 11:00:00","title":"Swathi Market","mobile":"8919480920","fcmtoken":"ePYfPrzXWiY:APA91bFdKnAg7IIBUwIC8sMyk2cMWWx_Y9X4dNgpWJ8yQWWGPA1MzYRC7YJlwKvMqvYvAKZtGE4e8jTC5bjw1g3G5l9v8b7HnuA4-fZxbFJzY8i8SdO8MP0yccg75bAksrV37xUUrOQW"},{"orderid":"54","storeid":"1","bookingdatetime":"2020-05-13 22:03:42","finalbill":"892.32","invoiceno":"ST000200000000054","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-14 11:00:00","title":"Swathi Market","mobile":"8919480920","fcmtoken":"ePYfPrzXWiY:APA91bFdKnAg7IIBUwIC8sMyk2cMWWx_Y9X4dNgpWJ8yQWWGPA1MzYRC7YJlwKvMqvYvAKZtGE4e8jTC5bjw1g3G5l9v8b7HnuA4-fZxbFJzY8i8SdO8MP0yccg75bAksrV37xUUrOQW"},{"orderid":"90","storeid":"1","bookingdatetime":"2020-05-13 23:33:45","finalbill":"580.2","invoiceno":"ST000200000000090","paymentmode":"online","paymentsummary":"pay_EpvxUoy0n14Hty","paymentstatus":"Success","orderstatus":"Delivered","expectedtime":"2020-05-20 12:39:24","title":"Swathi Market","mobile":"8919480920","fcmtoken":"ePYfPrzXWiY:APA91bFdKnAg7IIBUwIC8sMyk2cMWWx_Y9X4dNgpWJ8yQWWGPA1MzYRC7YJlwKvMqvYvAKZtGE4e8jTC5bjw1g3G5l9v8b7HnuA4-fZxbFJzY8i8SdO8MP0yccg75bAksrV37xUUrOQW"},{"orderid":"97","storeid":"1","bookingdatetime":"2020-05-20 11:50:48","finalbill":"887.6","invoiceno":"ST000200000000097","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Confirmed","expectedtime":"2020-05-20 12:36:53","title":"Swathi Market","mobile":"8919480920","fcmtoken":"ePYfPrzXWiY:APA91bFdKnAg7IIBUwIC8sMyk2cMWWx_Y9X4dNgpWJ8yQWWGPA1MzYRC7YJlwKvMqvYvAKZtGE4e8jTC5bjw1g3G5l9v8b7HnuA4-fZxbFJzY8i8SdO8MP0yccg75bAksrV37xUUrOQW"},{"orderid":"98","storeid":"1","bookingdatetime":"2020-05-20 12:57:21","finalbill":"640.4","invoiceno":"ST000200000000098","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-21 11:00:00","title":"Swathi Market","mobile":"8919480920","fcmtoken":"ePYfPrzXWiY:APA91bFdKnAg7IIBUwIC8sMyk2cMWWx_Y9X4dNgpWJ8yQWWGPA1MzYRC7YJlwKvMqvYvAKZtGE4e8jTC5bjw1g3G5l9v8b7HnuA4-fZxbFJzY8i8SdO8MP0yccg75bAksrV37xUUrOQW"},{"orderid":"99","storeid":"1","bookingdatetime":"2020-05-20 13:12:17","finalbill":"2124.8","invoiceno":"ST000200000000099","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-21 11:00:00","title":"Swathi Market","mobile":"8919480920","fcmtoken":"ePYfPrzXWiY:APA91bFdKnAg7IIBUwIC8sMyk2cMWWx_Y9X4dNgpWJ8yQWWGPA1MzYRC7YJlwKvMqvYvAKZtGE4e8jTC5bjw1g3G5l9v8b7HnuA4-fZxbFJzY8i8SdO8MP0yccg75bAksrV37xUUrOQW"}]
     * message : success
     */

    private String message;
    private List<OrdersdataBean> ordersdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<OrdersdataBean> getOrdersdata() {
        return ordersdata;
    }

    public void setOrdersdata(List<OrdersdataBean> ordersdata) {
        this.ordersdata = ordersdata;
    }

    public static class OrdersdataBean {
        /**
         * orderid : 38
         * storeid : 1
         * bookingdatetime : 2020-05-12 17:24:11
         * finalbill : 672
         * invoiceno : ST000200000000038
         * paymentmode : Cod
         * paymentsummary : Cod
         * paymentstatus : Success
         * orderstatus : Pending
         * expectedtime : 2020-05-13 11:00:00
         * title : Swathi Market
         * mobile : 8919480920
         * fcmtoken : ePYfPrzXWiY:APA91bFdKnAg7IIBUwIC8sMyk2cMWWx_Y9X4dNgpWJ8yQWWGPA1MzYRC7YJlwKvMqvYvAKZtGE4e8jTC5bjw1g3G5l9v8b7HnuA4-fZxbFJzY8i8SdO8MP0yccg75bAksrV37xUUrOQW
         */

        private String orderid;
        private String storeid;
        private String bookingdatetime;
        private String finalbill;
        private String invoiceno;
        private String paymentmode;
        private String paymentsummary;
        private String paymentstatus;
        private String orderstatus;
        private String expectedtime;
        private String title;
        private String mobile;
        private String fcmtoken;

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getStoreid() {
            return storeid;
        }

        public void setStoreid(String storeid) {
            this.storeid = storeid;
        }

        public String getBookingdatetime() {
            return bookingdatetime;
        }

        public void setBookingdatetime(String bookingdatetime) {
            this.bookingdatetime = bookingdatetime;
        }

        public String getFinalbill() {
            return finalbill;
        }

        public void setFinalbill(String finalbill) {
            this.finalbill = finalbill;
        }

        public String getInvoiceno() {
            return invoiceno;
        }

        public void setInvoiceno(String invoiceno) {
            this.invoiceno = invoiceno;
        }

        public String getPaymentmode() {
            return paymentmode;
        }

        public void setPaymentmode(String paymentmode) {
            this.paymentmode = paymentmode;
        }

        public String getPaymentsummary() {
            return paymentsummary;
        }

        public void setPaymentsummary(String paymentsummary) {
            this.paymentsummary = paymentsummary;
        }

        public String getPaymentstatus() {
            return paymentstatus;
        }

        public void setPaymentstatus(String paymentstatus) {
            this.paymentstatus = paymentstatus;
        }

        public String getOrderstatus() {
            return orderstatus;
        }

        public void setOrderstatus(String orderstatus) {
            this.orderstatus = orderstatus;
        }

        public String getExpectedtime() {
            return expectedtime;
        }

        public void setExpectedtime(String expectedtime) {
            this.expectedtime = expectedtime;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getFcmtoken() {
            return fcmtoken;
        }

        public void setFcmtoken(String fcmtoken) {
            this.fcmtoken = fcmtoken;
        }
    }
}

package com.pivotasoft.selltezhygiene.Response;

import java.util.List;

public class CartResponse {


    /**
     * cartlistdata : [{"cartid":"2","buyerid":"20","storeid":"1","productid":"3","finalprice":"76","quantity":"1","itemtotal":"76","addedat":"2020-05-09 19:05:07","title":"Aasirwad Sugar Release COntrol Atta 1KG","measureunits":"1 KG","productpic":"aasirwad-sug-atta.png"},{"cartid":"3","buyerid":"20","storeid":"9","productid":"1","finalprice":"20","quantity":"1","itemtotal":"20","addedat":"2020-05-09 19:05:47","title":"Tajmahal Premium Idly Rawa 2Kg","measureunits":"2 Kg","productpic":"taj-p-rawa.png"}]
     * message : success
     */

    private String message;
    private List<CartlistdataBean> cartlistdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CartlistdataBean> getCartlistdata() {
        return cartlistdata;
    }

    public void setCartlistdata(List<CartlistdataBean> cartlistdata) {
        this.cartlistdata = cartlistdata;
    }

    public static class CartlistdataBean {
        /**
         * cartid : 2
         * buyerid : 20
         * storeid : 1
         * productid : 3
         * finalprice : 76
         * quantity : 1
         * itemtotal : 76
         * addedat : 2020-05-09 19:05:07
         * title : Aasirwad Sugar Release COntrol Atta 1KG
         * measureunits : 1 KG
         * productpic : aasirwad-sug-atta.png
         */

        private String cartid;
        private String buyerid;
        private String storeid;
        private String productid;
        private String finalprice;
        private String quantity;
        private String itemtotal;
        private String addedat;
        private String title;
        private String measureunits;
        private String productpic;

        public String getCartid() {
            return cartid;
        }

        public void setCartid(String cartid) {
            this.cartid = cartid;
        }

        public String getBuyerid() {
            return buyerid;
        }

        public void setBuyerid(String buyerid) {
            this.buyerid = buyerid;
        }

        public String getStoreid() {
            return storeid;
        }

        public void setStoreid(String storeid) {
            this.storeid = storeid;
        }

        public String getProductid() {
            return productid;
        }

        public void setProductid(String productid) {
            this.productid = productid;
        }

        public String getFinalprice() {
            return finalprice;
        }

        public void setFinalprice(String finalprice) {
            this.finalprice = finalprice;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getItemtotal() {
            return itemtotal;
        }

        public void setItemtotal(String itemtotal) {
            this.itemtotal = itemtotal;
        }

        public String getAddedat() {
            return addedat;
        }

        public void setAddedat(String addedat) {
            this.addedat = addedat;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMeasureunits() {
            return measureunits;
        }

        public void setMeasureunits(String measureunits) {
            this.measureunits = measureunits;
        }

        public String getProductpic() {
            return productpic;
        }

        public void setProductpic(String productpic) {
            this.productpic = productpic;
        }
    }
}

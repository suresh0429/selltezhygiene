package com.pivotasoft.selltezhygiene.Response;

import java.util.List;

public class CouponResponse {


    /**
     * couponsdata : [{"couponid":"1","couponcode":"MAY2020","storeid":"1","description":"Enjoy 10% Discount upto Rs. 100","mintransaction":"2000","discountpercent":"10","maxdiscount":"100","fromdate":"2020-05-01","todate":"2020-05-31"}]
     * message : success
     */

    private String message;
    private List<CouponsdataBean> couponsdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CouponsdataBean> getCouponsdata() {
        return couponsdata;
    }

    public void setCouponsdata(List<CouponsdataBean> couponsdata) {
        this.couponsdata = couponsdata;
    }

    public static class CouponsdataBean {
        /**
         * couponid : 1
         * couponcode : MAY2020
         * storeid : 1
         * description : Enjoy 10% Discount upto Rs. 100
         * mintransaction : 2000
         * discountpercent : 10
         * maxdiscount : 100
         * fromdate : 2020-05-01
         * todate : 2020-05-31
         */

        private String couponid;
        private String couponcode;
        private String storeid;
        private String description;
        private String mintransaction;
        private String discountpercent;
        private String maxdiscount;
        private String fromdate;
        private String todate;

        public String getCouponid() {
            return couponid;
        }

        public void setCouponid(String couponid) {
            this.couponid = couponid;
        }

        public String getCouponcode() {
            return couponcode;
        }

        public void setCouponcode(String couponcode) {
            this.couponcode = couponcode;
        }

        public String getStoreid() {
            return storeid;
        }

        public void setStoreid(String storeid) {
            this.storeid = storeid;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getMintransaction() {
            return mintransaction;
        }

        public void setMintransaction(String mintransaction) {
            this.mintransaction = mintransaction;
        }

        public String getDiscountpercent() {
            return discountpercent;
        }

        public void setDiscountpercent(String discountpercent) {
            this.discountpercent = discountpercent;
        }

        public String getMaxdiscount() {
            return maxdiscount;
        }

        public void setMaxdiscount(String maxdiscount) {
            this.maxdiscount = maxdiscount;
        }

        public String getFromdate() {
            return fromdate;
        }

        public void setFromdate(String fromdate) {
            this.fromdate = fromdate;
        }

        public String getTodate() {
            return todate;
        }

        public void setTodate(String todate) {
            this.todate = todate;
        }
    }
}

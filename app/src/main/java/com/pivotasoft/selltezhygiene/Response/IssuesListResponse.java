package com.pivotasoft.selltezhygiene.Response;

import java.util.List;

public class IssuesListResponse {


    /**
     * issuelistdata : [{"issueid":"7","orderid":"35","description":"It's getting late","assignedon":"2020-05-12 10:23:54","resolvedescription":"Pending","resolvedon":null,"buyerid":"1","bookingdatetime":"2020-05-12 15:51:51","billamount":"2053","couponid":"4","discount":"100","deliverycharges":"41.06","finalbill":"1994.06","addressid":"1","storeid":"2","invoiceno":"ST000010000000035","title":"Satya Super Market"}]
     * message : success
     */

    private String message;
    private List<IssuelistdataBean> issuelistdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<IssuelistdataBean> getIssuelistdata() {
        return issuelistdata;
    }

    public void setIssuelistdata(List<IssuelistdataBean> issuelistdata) {
        this.issuelistdata = issuelistdata;
    }

    public static class IssuelistdataBean {
        /**
         * issueid : 7
         * orderid : 35
         * description : It's getting late
         * assignedon : 2020-05-12 10:23:54
         * resolvedescription : Pending
         * resolvedon : null
         * buyerid : 1
         * bookingdatetime : 2020-05-12 15:51:51
         * billamount : 2053
         * couponid : 4
         * discount : 100
         * deliverycharges : 41.06
         * finalbill : 1994.06
         * addressid : 1
         * storeid : 2
         * invoiceno : ST000010000000035
         * title : Satya Super Market
         */

        private String issueid;
        private String orderid;
        private String description;
        private String assignedon;
        private String resolvedescription;
        private Object resolvedon;
        private String buyerid;
        private String bookingdatetime;
        private String billamount;
        private String couponid;
        private String discount;
        private String deliverycharges;
        private String finalbill;
        private String addressid;
        private String storeid;
        private String invoiceno;
        private String title;

        public String getIssueid() {
            return issueid;
        }

        public void setIssueid(String issueid) {
            this.issueid = issueid;
        }

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAssignedon() {
            return assignedon;
        }

        public void setAssignedon(String assignedon) {
            this.assignedon = assignedon;
        }

        public String getResolvedescription() {
            return resolvedescription;
        }

        public void setResolvedescription(String resolvedescription) {
            this.resolvedescription = resolvedescription;
        }

        public Object getResolvedon() {
            return resolvedon;
        }

        public void setResolvedon(Object resolvedon) {
            this.resolvedon = resolvedon;
        }

        public String getBuyerid() {
            return buyerid;
        }

        public void setBuyerid(String buyerid) {
            this.buyerid = buyerid;
        }

        public String getBookingdatetime() {
            return bookingdatetime;
        }

        public void setBookingdatetime(String bookingdatetime) {
            this.bookingdatetime = bookingdatetime;
        }

        public String getBillamount() {
            return billamount;
        }

        public void setBillamount(String billamount) {
            this.billamount = billamount;
        }

        public String getCouponid() {
            return couponid;
        }

        public void setCouponid(String couponid) {
            this.couponid = couponid;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getDeliverycharges() {
            return deliverycharges;
        }

        public void setDeliverycharges(String deliverycharges) {
            this.deliverycharges = deliverycharges;
        }

        public String getFinalbill() {
            return finalbill;
        }

        public void setFinalbill(String finalbill) {
            this.finalbill = finalbill;
        }

        public String getAddressid() {
            return addressid;
        }

        public void setAddressid(String addressid) {
            this.addressid = addressid;
        }

        public String getStoreid() {
            return storeid;
        }

        public void setStoreid(String storeid) {
            this.storeid = storeid;
        }

        public String getInvoiceno() {
            return invoiceno;
        }

        public void setInvoiceno(String invoiceno) {
            this.invoiceno = invoiceno;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}

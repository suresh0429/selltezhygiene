package com.pivotasoft.selltezhygiene.Response;

import java.util.List;

public class SubCatResponse
{


    /**
     * subcategoriesdata : [{"subcategoryid":"1","subcategoryname":"Sugar, Joggery and Salt","subcatpic":"sugar_jaggery.jpg","categoryid":"1"},{"subcategoryid":"2","subcategoryname":"Rice & Cereals","subcatpic":"cereals.jpg","categoryid":"1"},{"subcategoryid":"3","subcategoryname":"Dal & Flour","subcatpic":"dalflour.jpg","categoryid":"1"},{"subcategoryid":"4","subcategoryname":"Dry Fruits","subcatpic":"dryfruits.jpg","categoryid":"1"},{"subcategoryid":"5","subcategoryname":"Oil & Ghee","subcatpic":"oils.jpg","categoryid":"1"},{"subcategoryid":"6","subcategoryname":"Masala & Spice","subcatpic":"spices.jpg","categoryid":"1"}]
     * message : success
     */

    private String message;
    private List<SubcategoriesdataBean> subcategoriesdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SubcategoriesdataBean> getSubcategoriesdata() {
        return subcategoriesdata;
    }

    public void setSubcategoriesdata(List<SubcategoriesdataBean> subcategoriesdata) {
        this.subcategoriesdata = subcategoriesdata;
    }

    public static class SubcategoriesdataBean {
        /**
         * subcategoryid : 1
         * subcategoryname : Sugar, Joggery and Salt
         * subcatpic : sugar_jaggery.jpg
         * categoryid : 1
         */

        private String subcategoryid;
        private String subcategoryname;
        private String subcatpic;
        private String categoryid;

        public String getSubcategoryid() {
            return subcategoryid;
        }

        public void setSubcategoryid(String subcategoryid) {
            this.subcategoryid = subcategoryid;
        }

        public String getSubcategoryname() {
            return subcategoryname;
        }

        public void setSubcategoryname(String subcategoryname) {
            this.subcategoryname = subcategoryname;
        }

        public String getSubcatpic() {
            return subcatpic;
        }

        public void setSubcatpic(String subcatpic) {
            this.subcatpic = subcatpic;
        }

        public String getCategoryid() {
            return categoryid;
        }

        public void setCategoryid(String categoryid) {
            this.categoryid = categoryid;
        }
    }
}

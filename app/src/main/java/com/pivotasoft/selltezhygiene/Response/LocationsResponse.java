package com.pivotasoft.selltezhygiene.Response;

import java.util.List;

public class LocationsResponse {


    /**
     * status : 10100
     * message : Data Fetch successfully.
     * data : [{"pincode":"500033","area":"Jublee Hills","shipping_charges":"20"},{"pincode":"500007","area":"Habsiguda","shipping_charges":"50"}]
     */

    private String status;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * pincode : 500033
         * area : Jublee Hills
         * shipping_charges : 20
         */

        private String pincode;
        private String area;
        private String shipping_charges;

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getShipping_charges() {
            return shipping_charges;
        }

        public void setShipping_charges(String shipping_charges) {
            this.shipping_charges = shipping_charges;
        }
    }
}

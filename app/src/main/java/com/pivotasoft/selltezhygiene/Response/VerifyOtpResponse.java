package com.pivotasoft.selltezhygiene.Response;

public class VerifyOtpResponse {


    /**
     * status : 10100
     * message : You have been logged in successfully.
     * data : {"jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Njg3MDE5MDcsImp0aSI6Ik8rXC9XQzIySFBXdWRVTHJSOE80YURocER5Sm9jeGhMYWo5eHl5bG0yK1pRPSIsImlzcyI6Imh0dHA6XC9cL2lubmFzb2Z0LmluXC9waHAtanNvblwvIiwibmJmIjoxNTY4NzAxOTA4LCJleHAiOjE2MDAyMzc5MDgsImRhdGEiOnsidXNlcl9pZCI6IjEzIn19.7-e7WMrFMVxIKbWmLE2tL56iGwYQeGbw-dbWYfwdYZEaCf6fY4zlZvWUgD4mY2K9FUsvMeSXrFELpSfMWJRe-w","user_id":"13","user_name":"Bhavani","email":"bhavani@innasoft.in","mobile":"9542410774","gender":""}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * jwt : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Njg3MDE5MDcsImp0aSI6Ik8rXC9XQzIySFBXdWRVTHJSOE80YURocER5Sm9jeGhMYWo5eHl5bG0yK1pRPSIsImlzcyI6Imh0dHA6XC9cL2lubmFzb2Z0LmluXC9waHAtanNvblwvIiwibmJmIjoxNTY4NzAxOTA4LCJleHAiOjE2MDAyMzc5MDgsImRhdGEiOnsidXNlcl9pZCI6IjEzIn19.7-e7WMrFMVxIKbWmLE2tL56iGwYQeGbw-dbWYfwdYZEaCf6fY4zlZvWUgD4mY2K9FUsvMeSXrFELpSfMWJRe-w
         * user_id : 13
         * user_name : Bhavani
         * email : bhavani@innasoft.in
         * mobile : 9542410774
         * gender :
         */

        private String jwt;
        private String user_id;
        private String user_name;
        private String email;
        private String mobile;
        private String gender;

        public String getJwt() {
            return jwt;
        }

        public void setJwt(String jwt) {
            this.jwt = jwt;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }
    }
}

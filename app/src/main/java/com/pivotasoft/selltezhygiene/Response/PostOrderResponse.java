package com.pivotasoft.selltezhygiene.Response;

public class PostOrderResponse {


    /**
     * status : success
     * details : {"orderid":46,"message":"Order generated successfully"}
     */

    private String status;
    private DetailsBean details;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailsBean getDetails() {
        return details;
    }

    public void setDetails(DetailsBean details) {
        this.details = details;
    }

    public static class DetailsBean {
        /**
         * orderid : 46
         * message : Order generated successfully
         */

        private int orderid;
        private String message;

        public int getOrderid() {
            return orderid;
        }

        public void setOrderid(int orderid) {
            this.orderid = orderid;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}

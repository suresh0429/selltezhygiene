package com.pivotasoft.selltezhygiene.Response;

import java.util.List;

public class ProductSingleResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"id":"1","url_name":"toor-dal","product_name":"Toor Dal","type":"SINGLE","main_category_id":"1","main_category_name":"Staples","sub_category_id":"1","sub_category_name":"Dals And Pulses","child_category_id":"0","child_category_name":null,"unit_id":"1","unit_name":"Kg","unit_value":"2","brand_id":"1","brand_name":"KILOMART","qty":"10","mrp_price":"150.00","offer_price":"0","selling_price":"100.00","about":"kjbkj\n","moreinfo":"lknlk","availability":"0","user_rating":"0","features":"0","position":"1","seo_title":"toor-dal","seo_description":"Toor Dal","seo_keywords":"Toor Dal","images":["3bd75-toor-dal.jpeg"]}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * url_name : toor-dal
         * product_name : Toor Dal
         * type : SINGLE
         * main_category_id : 1
         * main_category_name : Staples
         * sub_category_id : 1
         * sub_category_name : Dals And Pulses
         * child_category_id : 0
         * child_category_name : null
         * unit_id : 1
         * unit_name : Kg
         * unit_value : 2
         * brand_id : 1
         * brand_name : KILOMART
         * qty : 10
         * mrp_price : 150.00
         * offer_price : 0
         * selling_price : 100.00
         * about : kjbkj
         * moreinfo : lknlk
         * availability : 0
         * user_rating : 0
         * features : 0
         * position : 1
         * seo_title : toor-dal
         * seo_description : Toor Dal
         * seo_keywords : Toor Dal
         * images : ["3bd75-toor-dal.jpeg"]
         */

        private String id;
        private String url_name;
        private String product_name;
        private String type;
        private String main_category_id;
        private String main_category_name;
        private String sub_category_id;
        private String sub_category_name;
        private String child_category_id;
        private Object child_category_name;
        private String unit_id;
        private String unit_name;
        private String unit_value;
        private String brand_id;
        private String brand_name;
        private String qty;
        private String mrp_price;
        private String offer_price;
        private String selling_price;
        private String about;
        private String moreinfo;
        private String availability;
        private String user_rating;
        private String features;
        private String position;
        private String seo_title;
        private String seo_description;
        private String seo_keywords;
        private List<String> images;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUrl_name() {
            return url_name;
        }

        public void setUrl_name(String url_name) {
            this.url_name = url_name;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMain_category_id() {
            return main_category_id;
        }

        public void setMain_category_id(String main_category_id) {
            this.main_category_id = main_category_id;
        }

        public String getMain_category_name() {
            return main_category_name;
        }

        public void setMain_category_name(String main_category_name) {
            this.main_category_name = main_category_name;
        }

        public String getSub_category_id() {
            return sub_category_id;
        }

        public void setSub_category_id(String sub_category_id) {
            this.sub_category_id = sub_category_id;
        }

        public String getSub_category_name() {
            return sub_category_name;
        }

        public void setSub_category_name(String sub_category_name) {
            this.sub_category_name = sub_category_name;
        }

        public String getChild_category_id() {
            return child_category_id;
        }

        public void setChild_category_id(String child_category_id) {
            this.child_category_id = child_category_id;
        }

        public Object getChild_category_name() {
            return child_category_name;
        }

        public void setChild_category_name(Object child_category_name) {
            this.child_category_name = child_category_name;
        }

        public String getUnit_id() {
            return unit_id;
        }

        public void setUnit_id(String unit_id) {
            this.unit_id = unit_id;
        }

        public String getUnit_name() {
            return unit_name;
        }

        public void setUnit_name(String unit_name) {
            this.unit_name = unit_name;
        }

        public String getUnit_value() {
            return unit_value;
        }

        public void setUnit_value(String unit_value) {
            this.unit_value = unit_value;
        }

        public String getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(String brand_id) {
            this.brand_id = brand_id;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getMrp_price() {
            return mrp_price;
        }

        public void setMrp_price(String mrp_price) {
            this.mrp_price = mrp_price;
        }

        public String getOffer_price() {
            return offer_price;
        }

        public void setOffer_price(String offer_price) {
            this.offer_price = offer_price;
        }

        public String getSelling_price() {
            return selling_price;
        }

        public void setSelling_price(String selling_price) {
            this.selling_price = selling_price;
        }

        public String getAbout() {
            return about;
        }

        public void setAbout(String about) {
            this.about = about;
        }

        public String getMoreinfo() {
            return moreinfo;
        }

        public void setMoreinfo(String moreinfo) {
            this.moreinfo = moreinfo;
        }

        public String getAvailability() {
            return availability;
        }

        public void setAvailability(String availability) {
            this.availability = availability;
        }

        public String getUser_rating() {
            return user_rating;
        }

        public void setUser_rating(String user_rating) {
            this.user_rating = user_rating;
        }

        public String getFeatures() {
            return features;
        }

        public void setFeatures(String features) {
            this.features = features;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getSeo_title() {
            return seo_title;
        }

        public void setSeo_title(String seo_title) {
            this.seo_title = seo_title;
        }

        public String getSeo_description() {
            return seo_description;
        }

        public void setSeo_description(String seo_description) {
            this.seo_description = seo_description;
        }

        public String getSeo_keywords() {
            return seo_keywords;
        }

        public void setSeo_keywords(String seo_keywords) {
            this.seo_keywords = seo_keywords;
        }

        public List<String> getImages() {
            return images;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }
    }
}

package com.pivotasoft.selltezhygiene.Response;

import java.util.List;

public class ProductResponse {
    /**
     * productsdata : [{"productid":"5","title":"Tata Iodised Crystal Salt","measureunits":"1 KG","unitprice":"14","discount":"1","finalprice":"13","productpic":"tata-crystal-salt.png","description":"Tata Iodised Crystal Salt","subcategoryid":"1","storeid":"1","status":"1"},{"productid":"6","title":"Aashirwad Iodised Salt","measureunits":"1 Kg","unitprice":"19","discount":"1","finalprice":"18","productpic":"aashirwad-salt.png","description":"Aashirwad Iodised Salt","subcategoryid":"1","storeid":"1","status":"1"},{"productid":"8","title":"Madhura Sugar","measureunits":"1 Kg","unitprice":"80","discount":"25","finalprice":"59","productpic":"madhura-sugar.png","description":"Madhura Sugar","subcategoryid":"1","storeid":"1","status":"2"}]
     * message : success
     */

    private String message;
    private List<ProductsdataBean> productsdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProductsdataBean> getProductsdata() {
        return productsdata;
    }

    public void setProductsdata(List<ProductsdataBean> productsdata) {
        this.productsdata = productsdata;
    }

    public static class ProductsdataBean {
        /**
         * productid : 5
         * title : Tata Iodised Crystal Salt
         * measureunits : 1 KG
         * unitprice : 14
         * discount : 1
         * finalprice : 13
         * productpic : tata-crystal-salt.png
         * description : Tata Iodised Crystal Salt
         * subcategoryid : 1
         * storeid : 1
         * status : 1
         */

        private String productid;
        private String title;
        private String measureunits;
        private String unitprice;
        private String discount;
        private String finalprice;
        private String productpic;
        private String description;
        private String subcategoryid;
        private String storeid;
        private String status;

        public String getProductid() {
            return productid;
        }

        public void setProductid(String productid) {
            this.productid = productid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMeasureunits() {
            return measureunits;
        }

        public void setMeasureunits(String measureunits) {
            this.measureunits = measureunits;
        }

        public String getUnitprice() {
            return unitprice;
        }

        public void setUnitprice(String unitprice) {
            this.unitprice = unitprice;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getFinalprice() {
            return finalprice;
        }

        public void setFinalprice(String finalprice) {
            this.finalprice = finalprice;
        }

        public String getProductpic() {
            return productpic;
        }

        public void setProductpic(String productpic) {
            this.productpic = productpic;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSubcategoryid() {
            return subcategoryid;
        }

        public void setSubcategoryid(String subcategoryid) {
            this.subcategoryid = subcategoryid;
        }

        public String getStoreid() {
            return storeid;
        }

        public void setStoreid(String storeid) {
            this.storeid = storeid;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }




    /* *//**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"id":"4","url_name":"chana-dal","product_name":"Chana Dal","type":"SINGLE","main_category_id":"1","main_category_name":"Staples","sub_category_id":"1","sub_category_name":"Dals And Pulses","child_category_id":"0","child_category_name":null,"unit_id":"1","unit_name":"Kg","unit_value":"2","brand_id":"1","brand_name":"KILOMART","qty":"10","mrp_price":"100.00","offer_price":"0","selling_price":"90.00","about":"Chana Dal","moreinfo":"Chana Dal","availability":"0","user_rating":"0","features":"0","position":"4","seo_title":"chana-dal","seo_description":"Chana Dal","seo_keywords":"Chana Dal","images":["64121-chana-dal.png"]},{"id":"1","url_name":"toor-dal","product_name":"Toor Dal","type":"SINGLE","main_category_id":"1","main_category_name":"Staples","sub_category_id":"1","sub_category_name":"Dals And Pulses","child_category_id":"0","child_category_name":null,"unit_id":"1","unit_name":"Kg","unit_value":"2","brand_id":"1","brand_name":"KILOMART","qty":"10","mrp_price":"150.00","offer_price":"0","selling_price":"100.00","about":"kjbkj\n","moreinfo":"lknlk","availability":"0","user_rating":"0","features":"0","position":"1","seo_title":"toor-dal","seo_description":"Toor Dal","seo_keywords":"Toor Dal","images":["3bd75-toor-dal.jpeg"]},{"id":"2","url_name":"urad-dal","product_name":"Urad Dal","type":"SINGLE","main_category_id":"1","main_category_name":"Staples","sub_category_id":"1","sub_category_name":"Dals And Pulses","child_category_id":"0","child_category_name":null,"unit_id":"1","unit_name":"Kg","unit_value":"2","brand_id":"1","brand_name":"KILOMART","qty":"10","mrp_price":"110.00","offer_price":"0","selling_price":"100.00","about":"Urad Dal","moreinfo":"Urad Dal","availability":"0","user_rating":"0","features":"0","position":"2","seo_title":"urad-dal","seo_description":"Urad Dal","seo_keywords":"Urad Dal","images":["bc8f1-urad-dal.png"]},{"id":"3","url_name":"moong-dal","product_name":"Moong dal","type":"SINGLE","main_category_id":"1","main_category_name":"Staples","sub_category_id":"1","sub_category_name":"Dals And Pulses","child_category_id":"0","child_category_name":null,"unit_id":"1","unit_name":"Kg","unit_value":"2","brand_id":"1","brand_name":"KILOMART","qty":"10","mrp_price":"200.00","offer_price":"0","selling_price":"180.00","about":"Moong dal","moreinfo":"Moong dal","availability":"0","user_rating":"0","features":"0","position":"3","seo_title":"moong-dal","seo_description":"Moong dal","seo_keywords":"Moong dal","images":["d3e2a-moong-dal.png"]}]
     *//*

    private String status;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        *//**
         * id : 4
         * url_name : chana-dal
         * product_name : Chana Dal
         * type : SINGLE
         * main_category_id : 1
         * main_category_name : Staples
         * sub_category_id : 1
         * sub_category_name : Dals And Pulses
         * child_category_id : 0
         * child_category_name : null
         * unit_id : 1
         * unit_name : Kg
         * unit_value : 2
         * brand_id : 1
         * brand_name : KILOMART
         * qty : 10
         * mrp_price : 100.00
         * offer_price : 0
         * selling_price : 90.00
         * about : Chana Dal
         * moreinfo : Chana Dal
         * availability : 0
         * user_rating : 0
         * features : 0
         * position : 4
         * seo_title : chana-dal
         * seo_description : Chana Dal
         * seo_keywords : Chana Dal
         * images : ["64121-chana-dal.png"]
         *//*

        private String id;
        private String url_name;
        private String product_name;
        private String type;
        private String main_category_id;
        private String main_category_name;
        private String sub_category_id;
        private String sub_category_name;
        private String child_category_id;
        private Object child_category_name;
        private String unit_id;
        private String unit_name;
        private String unit_value;
        private String brand_id;
        private String brand_name;
        private String qty;
        private String mrp_price;
        private String offer_price;
        private String selling_price;
        private String about;
        private String moreinfo;
        private String availability;
        private String user_rating;
        private String features;
        private String position;
        private String seo_title;
        private String seo_description;
        private String seo_keywords;
        private List<String> images;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUrl_name() {
            return url_name;
        }

        public void setUrl_name(String url_name) {
            this.url_name = url_name;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMain_category_id() {
            return main_category_id;
        }

        public void setMain_category_id(String main_category_id) {
            this.main_category_id = main_category_id;
        }

        public String getMain_category_name() {
            return main_category_name;
        }

        public void setMain_category_name(String main_category_name) {
            this.main_category_name = main_category_name;
        }

        public String getSub_category_id() {
            return sub_category_id;
        }

        public void setSub_category_id(String sub_category_id) {
            this.sub_category_id = sub_category_id;
        }

        public String getSub_category_name() {
            return sub_category_name;
        }

        public void setSub_category_name(String sub_category_name) {
            this.sub_category_name = sub_category_name;
        }

        public String getChild_category_id() {
            return child_category_id;
        }

        public void setChild_category_id(String child_category_id) {
            this.child_category_id = child_category_id;
        }

        public Object getChild_category_name() {
            return child_category_name;
        }

        public void setChild_category_name(Object child_category_name) {
            this.child_category_name = child_category_name;
        }

        public String getUnit_id() {
            return unit_id;
        }

        public void setUnit_id(String unit_id) {
            this.unit_id = unit_id;
        }

        public String getUnit_name() {
            return unit_name;
        }

        public void setUnit_name(String unit_name) {
            this.unit_name = unit_name;
        }

        public String getUnit_value() {
            return unit_value;
        }

        public void setUnit_value(String unit_value) {
            this.unit_value = unit_value;
        }

        public String getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(String brand_id) {
            this.brand_id = brand_id;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getMrp_price() {
            return mrp_price;
        }

        public void setMrp_price(String mrp_price) {
            this.mrp_price = mrp_price;
        }

        public String getOffer_price() {
            return offer_price;
        }

        public void setOffer_price(String offer_price) {
            this.offer_price = offer_price;
        }

        public String getSelling_price() {
            return selling_price;
        }

        public void setSelling_price(String selling_price) {
            this.selling_price = selling_price;
        }

        public String getAbout() {
            return about;
        }

        public void setAbout(String about) {
            this.about = about;
        }

        public String getMoreinfo() {
            return moreinfo;
        }

        public void setMoreinfo(String moreinfo) {
            this.moreinfo = moreinfo;
        }

        public String getAvailability() {
            return availability;
        }

        public void setAvailability(String availability) {
            this.availability = availability;
        }

        public String getUser_rating() {
            return user_rating;
        }

        public void setUser_rating(String user_rating) {
            this.user_rating = user_rating;
        }

        public String getFeatures() {
            return features;
        }

        public void setFeatures(String features) {
            this.features = features;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getSeo_title() {
            return seo_title;
        }

        public void setSeo_title(String seo_title) {
            this.seo_title = seo_title;
        }

        public String getSeo_description() {
            return seo_description;
        }

        public void setSeo_description(String seo_description) {
            this.seo_description = seo_description;
        }

        public String getSeo_keywords() {
            return seo_keywords;
        }

        public void setSeo_keywords(String seo_keywords) {
            this.seo_keywords = seo_keywords;
        }

        public List<String> getImages() {
            return images;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }
    }*/
}

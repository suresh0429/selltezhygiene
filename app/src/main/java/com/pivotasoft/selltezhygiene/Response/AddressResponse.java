package com.pivotasoft.selltezhygiene.Response;

import java.util.List;

public class AddressResponse {


    /**
     * addressdata : [{"addressid":"3","title":"suresh","laneandbuilding":"3-58, balaji nagar","landmark":"ysr statue","city":"hyderabad","state":"telangana","buyerid":"20"}]
     * message : success
     */

    private String message;
    private List<AddressdataBean> addressdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AddressdataBean> getAddressdata() {
        return addressdata;
    }

    public void setAddressdata(List<AddressdataBean> addressdata) {
        this.addressdata = addressdata;
    }

    public static class AddressdataBean {
        /**
         * addressid : 3
         * title : suresh
         * laneandbuilding : 3-58, balaji nagar
         * landmark : ysr statue
         * city : hyderabad
         * state : telangana
         * buyerid : 20
         */

        private String addressid;
        private String title;
        private String laneandbuilding;
        private String landmark;
        private String city;
        private String state;
        private String buyerid;

        public String getAddressid() {
            return addressid;
        }

        public void setAddressid(String addressid) {
            this.addressid = addressid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLaneandbuilding() {
            return laneandbuilding;
        }

        public void setLaneandbuilding(String laneandbuilding) {
            this.laneandbuilding = laneandbuilding;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getBuyerid() {
            return buyerid;
        }

        public void setBuyerid(String buyerid) {
            this.buyerid = buyerid;
        }
    }
}

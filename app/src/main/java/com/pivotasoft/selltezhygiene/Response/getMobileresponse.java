package com.pivotasoft.selltezhygiene.Response;

import java.util.List;

public class getMobileresponse {


    /**
     * userdata : [{"buyerid":"20","userkey":"jON128k3Y76JcFuzsHnhpDofULbVM490"}]
     * message : member exists
     */

    private String message;
    private List<UserdataBean> userdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserdataBean> getUserdata() {
        return userdata;
    }

    public void setUserdata(List<UserdataBean> userdata) {
        this.userdata = userdata;
    }

    public static class UserdataBean {
        /**
         * buyerid : 20
         * userkey : jON128k3Y76JcFuzsHnhpDofULbVM490
         */

        private String buyerid;
        private String userkey;

        public String getBuyerid() {
            return buyerid;
        }

        public void setBuyerid(String buyerid) {
            this.buyerid = buyerid;
        }

        public String getUserkey() {
            return userkey;
        }

        public void setUserkey(String userkey) {
            this.userkey = userkey;
        }
    }
}

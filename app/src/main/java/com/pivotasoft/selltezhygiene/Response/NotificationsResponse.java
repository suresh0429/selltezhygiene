package com.pivotasoft.selltezhygiene.Response;

import java.util.List;

public class NotificationsResponse {


    /**
     * notifications_data : [{"notificationid":"1","content":"Hi Selltez Consumer! Your order is accepted by Swathi Market. Your order will be deliverd in time","buyerid":"1","storeid":"0","msgtime":"2020-05-20 10:10:20"}]
     * message : success
     */

    private String message;
    private List<NotificationsDataBean> notifications_data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NotificationsDataBean> getNotifications_data() {
        return notifications_data;
    }

    public void setNotifications_data(List<NotificationsDataBean> notifications_data) {
        this.notifications_data = notifications_data;
    }

    public static class NotificationsDataBean {
        /**
         * notificationid : 1
         * content : Hi Selltez Consumer! Your order is accepted by Swathi Market. Your order will be deliverd in time
         * buyerid : 1
         * storeid : 0
         * msgtime : 2020-05-20 10:10:20
         */

        private String notificationid;
        private String content;
        private String buyerid;
        private String storeid;
        private String msgtime;

        public String getNotificationid() {
            return notificationid;
        }

        public void setNotificationid(String notificationid) {
            this.notificationid = notificationid;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getBuyerid() {
            return buyerid;
        }

        public void setBuyerid(String buyerid) {
            this.buyerid = buyerid;
        }

        public String getStoreid() {
            return storeid;
        }

        public void setStoreid(String storeid) {
            this.storeid = storeid;
        }

        public String getMsgtime() {
            return msgtime;
        }

        public void setMsgtime(String msgtime) {
            this.msgtime = msgtime;
        }
    }
}

package com.pivotasoft.selltezhygiene.Response;

public class WishListDeleteresponse {


    /**
     * status : Item Deleted Successfully From WishList
     * message : Item Deleted Successfully from WishList
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

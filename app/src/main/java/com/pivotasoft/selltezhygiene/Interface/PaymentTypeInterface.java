package com.pivotasoft.selltezhygiene.Interface;

import com.pivotasoft.selltezhygiene.Response.CheckoutResponse;

import java.util.List;

public interface PaymentTypeInterface {

    void onItemClick(List<CheckoutResponse.DataBean.PaymentGatewayBean> paymentGatewayBean, int position);
}

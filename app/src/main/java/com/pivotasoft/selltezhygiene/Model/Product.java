package com.pivotasoft.selltezhygiene.Model;

public class Product {
    private String cartid;
    private String buyerid;
    private String storeid;
    private String productid;
    private int finalprice;
    private int quantity;
    private int itemtotal;
    private String addedat;
    private String title;
    private String measureunits;
    private String productpic;


    public Product(String cartid, String buyerid, String storeid, String productid, int finalprice, int quantity, int itemtotal, String addedat, String title, String measureunits, String productpic) {
        this.cartid = cartid;
        this.buyerid = buyerid;
        this.storeid = storeid;
        this.productid = productid;
        this.finalprice = finalprice;
        this.quantity = quantity;
        this.itemtotal = itemtotal;
        this.addedat = addedat;
        this.title = title;
        this.measureunits = measureunits;
        this.productpic = productpic;
    }

    public String getCartid() {
        return cartid;
    }

    public void setCartid(String cartid) {
        this.cartid = cartid;
    }

    public String getBuyerid() {
        return buyerid;
    }

    public void setBuyerid(String buyerid) {
        this.buyerid = buyerid;
    }

    public String getStoreid() {
        return storeid;
    }

    public void setStoreid(String storeid) {
        this.storeid = storeid;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public int getFinalprice() {
        return finalprice;
    }

    public void setFinalprice(int finalprice) {
        this.finalprice = finalprice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getItemtotal() {
        return itemtotal;
    }

    public void setItemtotal(int itemtotal) {
        this.itemtotal = itemtotal;
    }

    public String getAddedat() {
        return addedat;
    }

    public void setAddedat(String addedat) {
        this.addedat = addedat;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMeasureunits() {
        return measureunits;
    }

    public void setMeasureunits(String measureunits) {
        this.measureunits = measureunits;
    }

    public String getProductpic() {
        return productpic;
    }

    public void setProductpic(String productpic) {
        this.productpic = productpic;
    }
}

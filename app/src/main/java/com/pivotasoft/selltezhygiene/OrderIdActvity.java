package com.pivotasoft.selltezhygiene;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pivotasoft.selltezhygiene.Adapters.OrderIdAdapter;
import com.pivotasoft.selltezhygiene.Apis.ApiPush;
import com.pivotasoft.selltezhygiene.Apis.PushClient;
import com.pivotasoft.selltezhygiene.Apis.RetrofitClient;
import com.pivotasoft.selltezhygiene.Interface.Dilogueinterface;
import com.pivotasoft.selltezhygiene.Model.OrderIdItem;
import com.pivotasoft.selltezhygiene.Response.Data;
import com.pivotasoft.selltezhygiene.Response.IssuesResponse;
import com.pivotasoft.selltezhygiene.Response.NotificationBody;
import com.pivotasoft.selltezhygiene.Response.OrdersResponse;
import com.pivotasoft.selltezhygiene.Singleton.AppController;
import com.pivotasoft.selltezhygiene.Storage.PrefManager;
import com.pivotasoft.selltezhygiene.Storage.Utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderIdActvity extends AppCompatActivity implements Dilogueinterface {
    int color = Color.RED;
    int code;
    AppController appController;
    @BindView(R.id.orderidRecycler)
    RecyclerView orderidRecycler;
    @BindView(R.id.txtError)
    TextView txtError;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.rbCurrent)
    RadioButton rbCurrent;
    @BindView(R.id.rbFinished)
    RadioButton rbFinished;
    @BindView(R.id.filter)
    RadioGroup filter;
    private PrefManager pref;
    OrderIdAdapter myOrderAdapter;
    String userId, tokenValue, deviceId, name, storeId, storeName, storeFcmKey;
    boolean checkoutStatus;
    Dilogueinterface dilogueinterface;
    private ArrayList<OrderIdItem> orderIdItemsCurrent = new ArrayList<>();
    private ArrayList<OrderIdItem> orderIdItemsFinised = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_id_actvity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Orders");
        appController = (AppController) getApplication();

        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        name = profile.get("name");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        dilogueinterface = (Dilogueinterface) this;

        if (getIntent().getExtras() != null) {
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);
            storeId = getIntent().getStringExtra("storeId");
            storeName = getIntent().getStringExtra("storeName");
            storeFcmKey = getIntent().getStringExtra("storeFcmKey");

        }


        if (appController.isConnection()) {

            prepareOrderIdData("Current");
            filter.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int selectedId = filter.getCheckedRadioButtonId();

                    // find the radiobutton by returned id
                    RadioButton radioButton = (RadioButton) findViewById(selectedId);
                    if (radioButton.getText().equals("Current")) {
                        prepareOrderIdData("Current");

                    } else if (radioButton.getText().equals("Finished")) {
                        prepareOrderIdData("Finished");

                    }
                }
            });




        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }
    }


    private void prepareOrderIdData(String filter) {

        progressBar.setVisibility(View.VISIBLE);

        Call<OrdersResponse> call = RetrofitClient.getInstance().getApi().ordersIdList(tokenValue, userId);
        call.enqueue(new Callback<OrdersResponse>() {
            @Override
            public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                if (response.isSuccessful()) {

                    progressBar.setVisibility(View.GONE);
                    OrdersResponse myOrdersResponse = response.body();

                    List<OrdersResponse.OrdersdataBean> modulesBeanList = response.body().getOrdersdata();

                    orderIdItemsCurrent.clear();
                    orderIdItemsFinised.clear();
                    for (OrdersResponse.OrdersdataBean ordersdataBean : modulesBeanList) {

                        if (ordersdataBean.getOrderstatus().equalsIgnoreCase("Rejected") || ordersdataBean.getOrderstatus().equalsIgnoreCase("Confirmed")) {
                            orderIdItemsFinised.add(new OrderIdItem(ordersdataBean.getOrderid(),ordersdataBean.getStoreid(),ordersdataBean.getBookingdatetime(),
                                    ordersdataBean.getFinalbill(),ordersdataBean.getInvoiceno(),ordersdataBean.getPaymentmode(),ordersdataBean.getPaymentsummary(),
                                    ordersdataBean.getPaymentstatus(),ordersdataBean.getOrderstatus(),ordersdataBean.getExpectedtime(),ordersdataBean.getTitle(),ordersdataBean.getMobile(),
                                    ordersdataBean.getFcmtoken()));
                        } else {
                            orderIdItemsCurrent.add(new OrderIdItem(ordersdataBean.getOrderid(),ordersdataBean.getStoreid(),ordersdataBean.getBookingdatetime(),
                                    ordersdataBean.getFinalbill(),ordersdataBean.getInvoiceno(),ordersdataBean.getPaymentmode(),ordersdataBean.getPaymentsummary(),
                                    ordersdataBean.getPaymentstatus(),ordersdataBean.getOrderstatus(),ordersdataBean.getExpectedtime(),ordersdataBean.getTitle(),ordersdataBean.getMobile(),
                                    ordersdataBean.getFcmtoken()));
                        }
                    }

                    if (filter.equalsIgnoreCase("Current")){

                        if (orderIdItemsCurrent.size() != 0){
                            txtError.setVisibility(View.GONE);
                        }else {
                            txtError.setVisibility(View.VISIBLE);
                            txtError.setText("No Orders Found !");
                        }

                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        layoutManager.setReverseLayout(true);
                        layoutManager.setStackFromEnd(true);
                        orderidRecycler.setItemAnimator(new DefaultItemAnimator());
                        orderidRecycler.setLayoutManager(layoutManager);
                        myOrderAdapter = new OrderIdAdapter(getApplicationContext(), orderIdItemsCurrent, dilogueinterface,storeName,storeFcmKey);
                        orderidRecycler.setAdapter(myOrderAdapter);
                        myOrderAdapter.notifyDataSetChanged();
                    }else {

                        if (orderIdItemsFinised.size() != 0){
                            txtError.setVisibility(View.GONE);
                        }
                        else {
                            progressBar.setVisibility(View.GONE);
                            txtError.setVisibility(View.VISIBLE);
                            txtError.setText("No Orders Found !");
                        }


                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        layoutManager.setReverseLayout(true);
                        layoutManager.setStackFromEnd(true);
                        orderidRecycler.setItemAnimator(new DefaultItemAnimator());
                        orderidRecycler.setLayoutManager(layoutManager);
                        myOrderAdapter = new OrderIdAdapter(getApplicationContext(), orderIdItemsFinised, dilogueinterface,storeName,storeFcmKey);
                        orderidRecycler.setAdapter(myOrderAdapter);
                        myOrderAdapter.notifyDataSetChanged();
                    }

                } else {
                    progressBar.setVisibility(View.GONE);
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("No Orders Found !");
                }
            }

            @Override
            public void onFailure(Call<OrdersResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toasty.normal(OrderIdActvity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }


    @Override
    public void onDilogClick(final OrderIdItem orderListBean) {
        // send otp to your
        code = new Random().nextInt(900000) + 100000;
        Log.d("TAG", "otpSend: " + code);
        String message = "Thank you for choosing Selltez. Your Confirmation OTP is " + code;
        String urlString = "http://app.smsmoon.com/submitsms.jsp?user=PIVOTAL&key=7d9a0596c8XX&mobile=" + orderListBean.getMobile() + "&message=" + message + "&senderid=PVOTAL&accusage=1";
        WebView webView = new WebView(getApplicationContext());
        webView.loadUrl(urlString);

        Toasty.success(getApplicationContext(), "OTP sent to your registered mobile no.", Toast.LENGTH_SHORT).show();

        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.diloge_rating, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        //finally creating the alert dialog and displaying it
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();


        final RatingBar ratingBar = (RatingBar) dialogView.findViewById(R.id.rating);
        final EditText etOtp = (EditText) dialogView.findViewById(R.id.etOTP);
        Button buttonOk = (Button) dialogView.findViewById(R.id.btnsubmit);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!etOtp.getText().toString().isEmpty()) {

                    Log.d("TAG", "onClick: " + code + "------" + etOtp.getText().toString());

                    if (code == Integer.parseInt(etOtp.getText().toString())) {

                        progressBar.setVisibility(View.VISIBLE);
                        String rating = "" + ratingBar.getRating();
                        // ratings api call
                        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().addReview(orderListBean.getStoreid(), orderListBean.getOrderid(), rating);
                        call.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {

                                    progressBar.setVisibility(View.GONE);

                                    // Toasty.success(getApplicationContext(), "Review Added Successfully .", Toast.LENGTH_SHORT).show();
                                    alertDialog.dismiss();

                                } else {
                                    progressBar.setVisibility(View.GONE);

                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressBar.setVisibility(View.GONE);
                                Toasty.normal(OrderIdActvity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });


                        // update status api call
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        String currentDateandTime = sdf.format(new Date());

                        Call<ResponseBody> callUpdate = RetrofitClient.getInstance().getApi().updateOrderStatus("Confirmed", orderListBean.getOrderid(), currentDateandTime, tokenValue);
                        callUpdate.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    progressBar.setVisibility(View.GONE);
                                    ResponseBody myOrdersResponse = response.body();

                                    Toasty.success(getApplicationContext(), "OTP  Verified Successfully", Toast.LENGTH_SHORT).show();
                                    alertDialog.dismiss();
                                    prepareOrderIdData("Current");
                                    sendNotificationToPatner(orderListBean.getFcmtoken(), orderListBean.getInvoiceno(), ratingBar.getRating(), orderListBean.getStoreid());

                                } else {
                                    progressBar.setVisibility(View.GONE);

                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressBar.setVisibility(View.GONE);
                                Toasty.normal(OrderIdActvity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        Toasty.error(getApplicationContext(), "Please Enter Valid OTP ", Toasty.LENGTH_SHORT).show();
                    }

                } else {
                    Toasty.error(getApplicationContext(), "Please Enter  OTP ", Toasty.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void sendNotificationToPatner(String fcmtoken, String invoiceno, float rating, String storeid) {

        SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);

        String title = "Selltez";
        String content = "";
        if (rating > 4) {
            content = "Hi Selltez Consumer! Your order with Invoice#" + invoiceno + " delivered and confirmed by " + name + " with " + rating + " stars. Great going.";
        } else {
            content = "Hi Selltez Consumer! Your order with Invoice#" + invoiceno + " delivered and confirmed by " + name + " with " + rating + " stars. Please improve your service.";

        }

        String to = fcmtoken;
        Log.d("TAG", "sendNotificationToPatner: " + fcmtoken);

        Data data = new Data(title, content);
        NotificationBody body = new NotificationBody(data, to);

        ApiPush apiService = PushClient.getClient().create(ApiPush.class);
        Call<ResponseBody> responseBodyCall = apiService.sendNotification(body);
        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("TAG", "Successfully notification send by using retrofit.");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        saveNotification(content, storeid);

    }

    private void saveNotification(String content, String storeid) {
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().saveNotification("0", tokenValue, storeid, content);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    ResponseBody myOrdersResponse = response.body();
                    Log.d("TAG", "onResponse: " + "success");

                } else {
                    progressBar.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toasty.normal(OrderIdActvity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDilogIssuesClick(final OrderIdItem orderListBean) {

        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.diloge_issues, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        //finally creating the alert dialog and displaying it
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();


        final EditText etDescription = (EditText) dialogView.findViewById(R.id.etDescription);
        Button buttonOk = (Button) dialogView.findViewById(R.id.btnsubmit);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!etDescription.getText().toString().isEmpty()) {

                    progressBar.setVisibility(View.VISIBLE);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    String currentDateandTime = sdf.format(new Date());

                    Call<IssuesResponse> call = RetrofitClient.getInstance().getApi().addIssue(etDescription.getText().toString(), orderListBean.getOrderid(), currentDateandTime, tokenValue);
                    call.enqueue(new Callback<IssuesResponse>() {
                        @Override
                        public void onResponse(Call<IssuesResponse> call, Response<IssuesResponse> response) {
                            if (response.isSuccessful()) {
                                progressBar.setVisibility(View.GONE);
                                IssuesResponse myOrdersResponse = response.body();

                                Toasty.success(getApplicationContext(), myOrdersResponse.getIssuedata().getMessage(), Toast.LENGTH_SHORT).show();
                                keypadHide(OrderIdActvity.this);
                                alertDialog.dismiss();

                            } else {
                                progressBar.setVisibility(View.GONE);

                            }
                        }

                        @Override
                        public void onFailure(Call<IssuesResponse> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            Toasty.normal(OrderIdActvity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    Toasty.normal(getApplicationContext(), "Enter Issue !", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public static void keypadHide(Activity context) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(Objects.requireNonNull(context.getCurrentFocus()).getWindowToken(), 0);
        } catch (Exception ignored) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                Intent intent = new Intent(OrderIdActvity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Checkout", checkoutStatus);
                intent.putExtra("storeId", storeId);
                intent.putExtra("storeName", storeName);
                intent.putExtra("storeFcmKey", storeFcmKey);
                startActivity(intent);

                break;


        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(OrderIdActvity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("Checkout", checkoutStatus);
        intent.putExtra("storeId", storeId);
        intent.putExtra("storeName", storeName);
        intent.putExtra("storeFcmKey", storeFcmKey);
        startActivity(intent);

    }
}

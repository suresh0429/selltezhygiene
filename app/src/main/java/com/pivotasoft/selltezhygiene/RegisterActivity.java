package com.pivotasoft.selltezhygiene;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pivotasoft.selltezhygiene.Apis.RetrofitClient;
import com.pivotasoft.selltezhygiene.Reciever.ConnectivityReceiver;
import com.pivotasoft.selltezhygiene.Response.getMobileresponse;
import com.pivotasoft.selltezhygiene.Singleton.AppController;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.pivotasoft.selltezhygiene.Apis.RetrofitClient.PRIVACY_POLICY_URL;
import static com.pivotasoft.selltezhygiene.Apis.RetrofitClient.TERMS_CONDITIONS_URL;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {
    boolean isConnected;
    String agree;
    String OTP;

    @BindView(R.id.imageView)
    AppCompatImageView imageView;
    @BindView(R.id.etUsername)
    TextInputEditText etUsername;
    @BindView(R.id.user_til)
    TextInputLayout userTil;
    @BindView(R.id.etPhone)
    TextInputEditText etPhone;
    @BindView(R.id.mobile_til)
    TextInputLayout mobileTil;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.email_til)
    TextInputLayout emailTil;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    @BindView(R.id.pwd_til)
    TextInputLayout pwdTil;
    @BindView(R.id.checkbox)
    CheckBox checkbox;
    @BindView(R.id.btnRegister)
    Button btnRegister;
    @BindView(R.id.txtSignin)
    TextView txtSignin;
    @BindView(R.id.txtTermscondition)
    TextView txtTermscondition;
    @BindView(R.id.parentLayout)
    NestedScrollView parentLayout;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    AppController app;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);


        app = (AppController) getApplication();

        String next = "Already Registered ? <font color='#f89113'>Login</font> me";
        txtSignin.setText(Html.fromHtml(next));

        String checktext = "I agree to the Kilomart ? <font color='#f89113'>Terms & conditions</font> and <font color='#f89113'>Privacy Policy</font>";
       // txtTermscondition.setText(Html.fromHtml(checktext));

        customTextView(txtTermscondition);

        agree = "0";
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    agree = "1";
                } else {
                    agree = "0";
                }
            }
        });


        etUsername.addTextChangedListener(new MyTextWatcher(etUsername));
        etPhone.addTextChangedListener(new MyTextWatcher(etPhone));
        etEmail.addTextChangedListener(new MyTextWatcher(etEmail));
        etPassword.addTextChangedListener(new MyTextWatcher(etPassword));


    }

    private void GetMobileNo(final String name, final String mobile, final String email, final String password){

        Call<getMobileresponse> call = RetrofitClient.getInstance().getApi().getMobileNo(etPhone.getText().toString());
        call.enqueue(new Callback<getMobileresponse>() {
            @Override
            public void onResponse(Call<getMobileresponse> call, Response<getMobileresponse> response) {

                if (response.isSuccessful()){
                    Intent intent = new Intent(getApplicationContext(), VerifyOtpActivity.class);
                    intent.putExtra("MobileNumber",mobile);
                    intent.putExtra("name",name);
                    intent.putExtra("email",email);
                    intent.putExtra("password",password);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else {
                    Toasty.normal(getApplicationContext(),"Mobile no already Exists.",Toast.LENGTH_SHORT).show();
                }



            }

            @Override
            public void onFailure(Call<getMobileresponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });

    }


    // user Registration
    private void userRegistartion(){

        final String name = etUsername.getText().toString().trim();
        final String mobile = etPhone.getText().toString().trim();
        final String email = etEmail.getText().toString().trim();
        final String password = etPassword.getText().toString().trim();

        if ((!isValidName(name))) {
            return;
        }
        if ((!isValidPhoneNumber(mobile))) {
            return;
        }
        if ((!isValidEmail(email))) {
            return;
        }
        if ((!isValidatePassword(password))) {
            return;
        }
        if (agree.equalsIgnoreCase("0")){
            Toasty.normal(getApplicationContext(),"Please Accept Terms & conditions",Toast.LENGTH_SHORT).show();
            return;
        }

        GetMobileNo(name,mobile,email,password);

    }




    @OnClick({R.id.btnRegister, R.id.txtSignin})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:

                // Method to manually check connection status
               boolean isConnected = app.isConnection();
                if (isConnected) {
                    userRegistartion();
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;
                    snackBar(message, color);
                }

                break;

            case R.id.txtSignin:
                Intent intent1 = new Intent(RegisterActivity.this, LoginActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            /*case R.id.txtVerify:
                // Method to manually check connection status
                isConnected = ConnectivityReceiver.isConnected();
                if (isConnected) {
                    userOtpRequest();
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;
                    snackBar(message, color);
                }

                break;*/
        }
    }



    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener

        AppController.getInstance().setConnectivityListener(this);
    }


    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }

        snackBar(message, color);


    }


    // snackBar
    private void snackBar(String message, int color) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }


    // validate name
    private boolean isValidName(String name) {
        Pattern pattern = Pattern.compile("[a-zA-Z ]+");
        Matcher matcher = pattern.matcher(name);

        if (name.isEmpty()) {
            userTil.setError("name is required");
            requestFocus(etUsername);
            return false;
        } else if (!matcher.matches()) {
            userTil.setError("Enter Alphabets Only");
            requestFocus(etUsername);
            return false;
        }
        else if (name.length() < 5 || name.length() > 20) {
            userTil.setError("Name Should be 5 to 20 characters");
            requestFocus(etUsername);
            return false;
        } else {
            userTil.setErrorEnabled(false);
        }

        return matcher.matches();
    }


    // validate phone
    private boolean isValidPhoneNumber(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            mobileTil.setError("Phone no is required");
            requestFocus(etPhone);

            return false;
        } else if (!matcher.matches()) {
            mobileTil.setError("Enter a valid mobile");
            requestFocus(etPhone);
            return false;
        } else {
            mobileTil.setErrorEnabled(false);
        }

        return matcher.matches();
    }



    // validate your email address
    private boolean isValidEmail(String email) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        if (email.isEmpty()) {
            emailTil.setError("Email is required");
            requestFocus(etEmail);
            return false;
        } else if (!matcher.matches()) {
            emailTil.setError("Enter a valid email");
            requestFocus(etEmail);
            return false;
        } else {
            emailTil.setErrorEnabled(false);
        }


        return matcher.matches();
    }



    // validate password
    private boolean isValidatePassword(String password) {
        if (password.isEmpty()) {
            pwdTil.setError("Password required");
            requestFocus(etPassword);
            return false;
        } else if (password.length()< 6 || password.length() > 20) {
            pwdTil.setError("Password Should be 6 to 20 characters");
            requestFocus(etPassword);
            return false;
        } else {
            pwdTil.setErrorEnabled(false);
        }

        return true;
    }


    // request focus
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    // text input layout class
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etUsername:
                    isValidName(etUsername.getText().toString().trim());
                    break;
                case R.id.etPhone:
                    isValidPhoneNumber(etPhone.getText().toString().trim());
                    break;
               /* case R.id.etOtp:
                    isValidOtp(etOtp.getText().toString().trim());
                    break;*/
                case R.id.etEmail:
                    isValidEmail(etEmail.getText().toString().trim());
                    break;
                case R.id.etPassword:
                    isValidatePassword(etPassword.getText().toString().trim());
                    break;
            }
        }
    }

    private void customTextView(TextView view) {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder(
                "I agree to the Selltez ");
        spanTxt.append("Terms & Conditions ");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {

               String title = "Terms & Conditions";
                Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
                intent.putExtra("title", title);
                intent.putExtra("url", TERMS_CONDITIONS_URL);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        }, spanTxt.length() - "Terms & Conditions ".length(), spanTxt.length(), 0);
        spanTxt.append(" and");
        spanTxt.setSpan(new ForegroundColorSpan(Color.BLACK), 32, spanTxt.length(), 0);
        spanTxt.append(" Privacy Policy");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {

                String title = "Privacy & Policy";
                Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
                intent.putExtra("title", title);
                intent.putExtra("url", PRIVACY_POLICY_URL);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        }, spanTxt.length() - " Privacy Policy".length(), spanTxt.length(), 0);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }
}

package com.pivotasoft.selltezhygiene;

import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pivotasoft.selltezhygiene.Adapters.AddressAdapter;
import com.pivotasoft.selltezhygiene.Apis.RetrofitClient;
import com.pivotasoft.selltezhygiene.Model.AddressModelItem;
import com.pivotasoft.selltezhygiene.Response.AddressResponse;
import com.pivotasoft.selltezhygiene.Singleton.AppController;
import com.pivotasoft.selltezhygiene.Storage.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressListActivity extends AppCompatActivity {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    LocationManager locationManager;

    @BindView(R.id.btnAddAddress)
    Button btnAddAddress;
    @BindView(R.id.addressList)
    ListView addressList;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.txtAlert)
    TextView txtAlert;

    private PrefManager pref;
    private AddressAdapter cartAdapter;

    String userId, itemslength, module, customerId, totalPrice, tokenValue, deviceId,storeId,storeName,storeFcmKey;
    int checked,grandTotal;
    boolean checkoutStatus;
    ArrayList<AddressModelItem> addressModelItems = new ArrayList<>();
    private String provider = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Address");


        AppController app = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        if (getIntent().getExtras() != null) {
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);
            grandTotal = getIntent().getIntExtra("grandTotal", 0);
            storeId = getIntent().getStringExtra("storeId");
            storeName = getIntent().getStringExtra("storeName");
            storeFcmKey = getIntent().getStringExtra("storeFcmKey");
            Log.d("TAG", "onCreate: "+grandTotal);

        }


        if (app.isConnection()) {

            prepareAddressData();


        } else {

            setContentView(R.layout.internet);


        }
    }

    private void prepareAddressData() {
        progress.setVisibility(View.VISIBLE);
        Call<AddressResponse> call = RetrofitClient.getInstance().getApi().addressList(tokenValue, userId);
        call.enqueue(new Callback<AddressResponse>() {
            @Override
            public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                progress.setVisibility(View.GONE);

                AddressResponse addressResponse = response.body();

//                Log.d("ADDRESS", "onResponse: " + addressResponse.getMessage());

                if (response.isSuccessful()) {

                    List<AddressResponse.AddressdataBean> docsBeanList = response.body() != null ? response.body().getAddressdata() : null;

                   /* for (AddressResponse.AddressdataBean address : docsBeanList) {

                        addressModelItems.add(new AddressModelItem(address.getAddressid(), address.getBuyerid(), address.getAddress_line1(), address.getAddress_line2(), address.getArea(), address.getCity(),
                                address.getState(), address.getPincode(), address.getCountry(), address.getContact_no(), address.getAlternate_contact_no(), address.getLatitude(), address.getLongitude(),
                                address.getIs_default(), address.isDelivery_status(), address.getDelivery_charges()));
                    }*/

                    cartAdapter = new AddressAdapter(AddressListActivity.this, docsBeanList, tokenValue, userId, checkoutStatus,grandTotal,storeId,storeName,storeFcmKey);
                    addressList.setAdapter(cartAdapter);
                    cartAdapter.setSelectedIndex(checked);
                    addressList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            cartAdapter.setSelectedIndex(position);
                            cartAdapter.notifyDataSetChanged();
                        }
                    });

                }else {
                    txtAlert.setVisibility(View.VISIBLE);

                    Toasty.success(getApplicationContext(),"No Address Found ",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddressResponse> call, Throwable t) {
                progress.setVisibility(View.GONE);
                Snackbar.make(addressList, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();

            }
        });


    }

    @OnClick(R.id.btnAddAddress)
    public void onViewClicked() {
        Intent intent = new Intent(AddressListActivity.this, AddAddressActivity.class);
        intent.putExtra("Checkout", checkoutStatus);
        intent.putExtra("grandTotal", grandTotal);
        intent.putExtra("storeId", storeId);
        intent.putExtra("storeName", storeName);
        intent.putExtra("storeFcmKey", storeFcmKey);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                if (checkoutStatus) {
                    Intent intent = new Intent(AddressListActivity.this, CartActivity.class);
                    intent.putExtra("Checkout", checkoutStatus);
                    intent.putExtra("grandTotal", grandTotal);
                    intent.putExtra("storeId", storeId);
                    intent.putExtra("storeName", storeName);
                    intent.putExtra("storeFcmKey", storeFcmKey);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(AddressListActivity.this, MyAccountActivity.class);
                    intent.putExtra("Checkout", checkoutStatus);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        if (checkoutStatus) {
            Intent intent = new Intent(AddressListActivity.this, CartActivity.class);
            intent.putExtra("Checkout", checkoutStatus);
            intent.putExtra("grandTotal", grandTotal);
            intent.putExtra("storeId", storeId);
            intent.putExtra("storeName", storeName);
            intent.putExtra("storeFcmKey", storeFcmKey);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Intent intent = new Intent(AddressListActivity.this, MyAccountActivity.class);
            intent.putExtra("Checkout", checkoutStatus);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}

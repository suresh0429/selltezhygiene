package com.pivotasoft.selltezhygiene;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pivotasoft.selltezhygiene.Apis.ApiPush;
import com.pivotasoft.selltezhygiene.Apis.PushClient;
import com.pivotasoft.selltezhygiene.Apis.RetrofitClient;
import com.pivotasoft.selltezhygiene.Response.CreateOrderResponse;
import com.pivotasoft.selltezhygiene.Response.Data;
import com.pivotasoft.selltezhygiene.Response.NotificationBody;
import com.pivotasoft.selltezhygiene.Singleton.AppController;
import com.pivotasoft.selltezhygiene.Storage.PrefManager;
import com.pivotasoft.selltezhygiene.Storage.Utilities;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.pivotasoft.selltezhygiene.Storage.Utilities.KEY_STORE_FCM;

public class RazorPayActivity extends AppCompatActivity implements PaymentResultListener {
    private static final String TAG = RazorPayActivity.class.getSimpleName();

    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;

    PrefManager pref;
    AppController appController;
    String module, email, mobile, tokenValue, name, userId, storeId, storeName,storeFcmKey, couponId, expectedtime;
    int orderId;
    Handler mHandler = new Handler();
    Double totalvalue,finalBill;
    private boolean checkInternet, checkout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_razor_pay);
        appController = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());

        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        name = profile.get("name");
        email = profile.get("email");
        mobile = profile.get("mobile");
        tokenValue = profile.get("AccessToken");
       // deviceId = profile.get("deviceId");

        if (appController.isConnection()) {

            if (getIntent().getExtras() != null) {
                orderId = getIntent().getIntExtra("orderId", 0);
                checkout = getIntent().getBooleanExtra("Checkout", false);
                storeId = getIntent().getStringExtra("storeId");
                storeName = getIntent().getStringExtra("storeName");
                storeFcmKey = getIntent().getStringExtra("storeFcmKey");
                finalBill = getIntent().getDoubleExtra("finalBill", 0.00);
                couponId = getIntent().getStringExtra("couponId");
                expectedtime = getIntent().getStringExtra("expectedtime");
                totalvalue = finalBill * 100;

            }

             /*
         To ensure faster loading of the Checkout form,
          call this method as early as possible in your checkout flow.
         */
            Checkout.preload(getApplicationContext());

            startPayment(orderId, String.format("%.2f", totalvalue));


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }

    }

    public void startPayment(int orderId, String total) {


        /*
          You need to pass current title in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();
/**
 * Set your logo here
 */
        co.setImage(R.mipmap.ic_launcher);
        try {
            JSONObject data = new JSONObject();
            data.put("name", "Selltez");
            data.put("description", "selltez");
            //You can omit the image option to fetch the image from dashboard
            data.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            data.put("currency", "INR");
            data.put("amount", total);

            JSONObject notes = new JSONObject();
            notes.put("email", email);
            notes.put("contact", mobile);
            notes.put("order_id", orderId);
            // notes.put("order_ref_no", order_ref_no);
            data.put("notes", notes);
            data.put("prefill", notes);

            co.open(activity, data);
        } catch (Exception e) {
            Toasty.error(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(final String razorpayPaymentID) {
        try {
            //Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            mHandler.post(new Runnable() {
                public void run() {
                    mHandler = null;

                    processOrder(orderId, razorpayPaymentID);


                }
            });

        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Log.d(TAG, "onPaymentError: " + code + "__" + response);
            Toast.makeText(getApplicationContext(),"Cancel payment" , Toast.LENGTH_SHORT).show();

             failedDilogue();
           /* if (code == 0) {
                Toast.makeText(this, response, Toast.LENGTH_SHORT).show();

                finish();

            } else {
                cancelOrder();
            }*/


        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    private void processOrder(int orderid, String razorpayPaymentID) {
//        progressLayout.setVisibility(View.VISIBLE);
        Call<CreateOrderResponse> call = RetrofitClient.getInstance().getApi().processOrder(tokenValue, userId, String.valueOf(orderid), couponId, "online", razorpayPaymentID, expectedtime);
        call.enqueue(new Callback<CreateOrderResponse>() {
            @Override
            public void onResponse(Call<CreateOrderResponse> call, Response<CreateOrderResponse> response) {

               // progressLayout.setVisibility(View.GONE);
                final CreateOrderResponse invoicePostResponse = response.body();

                if (response.isSuccessful()) {

                    successDilogue(invoicePostResponse.getDetails().getInvoiceno(), invoicePostResponse.getDetails().getMessage());

                    sendNotificationToPatner();

                    // cart update
                    appController.cartCount(userId, tokenValue);


                } else {

                }
            }

            @Override
            public void onFailure(Call<CreateOrderResponse> call, Throwable t) {
               // progressLayout.setVisibility(View.GONE);
            }
        });

    }

    private void sendNotificationToPatner() {
        SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);

        String title = "Selltez";
        String content = "Hi Selltez Member! An order amount of INR "+String.format("%.2f", totalvalue)+" placed by Mr. "+name+". Please check and confirm.";
        String to = preferences.getString(KEY_STORE_FCM,"");
        Log.d(TAG, "sendNotificationToPatner: "+preferences.getString(KEY_STORE_FCM,""));

        Data data = new Data(title, content);
        NotificationBody body = new NotificationBody(data, to);

        ApiPush apiService =  PushClient.getClient().create(ApiPush.class);
        Call<ResponseBody> responseBodyCall = apiService.sendNotification(body);
        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                Log.d(TAG,"Successfully notification send by using retrofit.");
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {

            }
        });

        saveNotification(content);

    }

    private void saveNotification(String content){
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().saveNotification("0",tokenValue,storeId,content);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    ResponseBody myOrdersResponse = response.body();
                    Log.d(TAG, "onResponse: "+"success");

                } else {
                    Log.d(TAG, "onResponse: "+"failure");

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toasty.normal(RazorPayActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void successDilogue(String razorpayPaymentID, String module) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(RazorPayActivity.this).inflate(R.layout.success_alert_diloge, viewGroup, false);

        TextView txttransactionId = dialogView.findViewById(R.id.txttransactionId);
        txttransactionId.setText("" + razorpayPaymentID);
        TextView txtOrderId = dialogView.findViewById(R.id.txtOrderId);
        txtOrderId.setText("" + orderId);

        Button btnOk = dialogView.findViewById(R.id.buttonOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent order_detail = new Intent(getApplicationContext(), OrderIdActvity.class);
                order_detail.putExtra("Checkout", checkout);
                order_detail.putExtra("storeId", storeId);
                order_detail.putExtra("storeName", storeName);
                startActivity(order_detail);
            }
        });
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(RazorPayActivity.this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void failedDilogue() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(RazorPayActivity.this).inflate(R.layout.failed_alert_diloge, viewGroup, false);

        Button btnOk = dialogView.findViewById(R.id.buttonOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(RazorPayActivity.this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void cancelOrder() {
//        progressLayout.setVisibility(View.VISIBLE);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().cancelOrder(tokenValue, String.valueOf(orderId));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

               // progressLayout.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    failedDilogue();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
               // progressLayout.setVisibility(View.GONE);
            }
        });

    }

}

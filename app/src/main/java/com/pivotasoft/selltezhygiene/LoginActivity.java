package com.pivotasoft.selltezhygiene;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.provider.Settings;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.pivotasoft.selltezhygiene.Apis.RetrofitClient;
import com.pivotasoft.selltezhygiene.Reciever.ConnectivityReceiver;
import com.pivotasoft.selltezhygiene.Response.LoginResponse;
import com.pivotasoft.selltezhygiene.Singleton.AppController;
import com.pivotasoft.selltezhygiene.Storage.PrefManager;
import com.pivotasoft.selltezhygiene.Storage.Utilities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = "LoginActivity";
    private static final String PREFS_LOCATION = "LOCATION_PREF";
    PrefManager session;
    boolean doubleBackToExitPressedOnce = false;
    Snackbar snackbar;
    @BindView(R.id.imageView)
    AppCompatImageView imageView;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    @BindView(R.id.txtForgotPassword)
    TextView txtForgotPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.txtSignup)
    TextView txtSignup;
    @BindView(R.id.parentLayout)
    LinearLayout parentLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.tilEmail)
    TextInputLayout tilEmail;
    @BindView(R.id.tilPassword)
    TextInputLayout tilPassword;

    String deviceID,refreshedToken;

    AppController app;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
         app = (AppController) getApplication();

        session = new PrefManager(this);
        if (session.isLoggedIn()) {
            startActivity(new Intent(LoginActivity.this, StroeLocationActivity.class));
            finish();
        }

        deviceID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        String next = "Not a member ? <font color='#f89113'>SignUp</font> now";
        txtSignup.setText(Html.fromHtml(next));

        etEmail.addTextChangedListener(new MyTextWatcher(etEmail));
        etPassword.addTextChangedListener(new MyTextWatcher(etPassword));




    }


    /* Validating form
     */
    private void submitForm() {

        if ((!isValidPhoneNumber(etEmail.getText().toString().trim()))) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        userLogin();

    }

    // retrofit login
    private void userLogin() {


        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();


        progressBar.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);

        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().userLogin(email, password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {


                progressBar.setVisibility(View.GONE);
                btnLogin.setEnabled(true);

                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    assert loginResponse != null;
                    if (loginResponse.getMessage().equals("success")) {

                        displayFirebaseRegId(loginResponse.getUserdata().get(0).getBuyerid(),loginResponse.getUserdata().get(0).getUserkey(),loginResponse);


                    }
                }
                else {

                    Toasty.error(LoginActivity.this, "Invalid username/password ", Toast.LENGTH_SHORT).show();

                    // updateFcmToken(loginResponse.getData().getJwt(), loginResponse.getData().getUser_id());

                }





            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                btnLogin.setEnabled(true);
            }
        });
    }





    public void setDefaults() {
        SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, true);
        editor.putString(Utilities.KEY_STORE_ID,"");
        editor.apply();
    }


    @OnClick({R.id.txtForgotPassword, R.id.btnLogin, R.id.txtSignup})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtForgotPassword:
                Intent intent2 = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent2);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.btnLogin:

                // Method to manually check connection status
                boolean isConnected = app.isConnection();

                if (isConnected) {
                    submitForm();
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;

                    snackBar(message, color);
                    //showSnack(isConnected);
                }

                break;
            case R.id.txtSignup:
                Intent intent1 = new Intent(LoginActivity.this, RegisterActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener

        AppController.getInstance().setConnectivityListener(this);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }

        snackBar(message, color);


    }

    // snackBar
    private void snackBar(String message, int color) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }


    // validate phone
//    private boolean isValidPhoneNumber(String mobile) {
//        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
//        Matcher matcher = pattern.matcher(mobile);
//
//        if (mobile.isEmpty()) {
//            tilPhone.setError("Phone no is required");
//            requestFocus(etPhone);
//            return false;
//        } else if (!matcher.matches()) {
//            tilPhone.setError("Enter a valid mobile");
//            requestFocus(etPhone);
//            return false;
//        } else {
//            tilPhone.setErrorEnabled(false);
//        }
//
//        return matcher.matches();
//    }

    // validate password
    private boolean validatePassword() {
        if (etPassword.getText().toString().trim().isEmpty()) {
            tilPassword.setError("Password required");
            requestFocus(etPassword);
            return false;
        } else if (etPassword.length() < 6) {
            tilPassword.setError("Password should be atleast 6 character long");
            requestFocus(etPassword);
            return false;
        } else {
            tilPassword.setErrorEnabled(false);
        }

        return true;
    }

    // request focus
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    // text input layout class
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etPhone:
                    isValidPhoneNumber(etEmail.getText().toString().trim());
                    break;
                case R.id.etPassword:
                    validatePassword();
                    break;
            }
        }
    }

    // validate phone
    private boolean isValidPhoneNumber(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            tilEmail.setError("Phone no is required");
            requestFocus(etEmail);

            return false;
        } else if (!matcher.matches()) {
            tilEmail.setError("Enter a valid mobile");
            requestFocus(etEmail);
            return false;
        } else {
            tilEmail.setErrorEnabled(false);
        }

        return matcher.matches();
    }


   /* private boolean isValidEmail(String email) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        if (email.isEmpty()) {
            tilEmail.setError("Email is required");
            requestFocus(etEmail);
            return false;
        } else if (!matcher.matches()) {
            tilEmail.setError("Enter a valid email");
            requestFocus(etEmail);
            return false;
        } else {
            tilEmail.setErrorEnabled(false);
        }


        return matcher.matches();
    }*/


    private void displayFirebaseRegId(String user_id, String userkey, LoginResponse loginResponse) {
       // SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.SHARED_PREF, 0);
        // String regId = pref.getString("regId", null);
        // Fcm Token
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TAG", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        refreshedToken = task.getResult().getToken();

                        // Log and toast

                        Log.d("TAG", refreshedToken);
                        // Toast.makeText(MainActivity.this, refreshedToken, Toast.LENGTH_SHORT).show();

                        Log.w("notificationToken", "" + refreshedToken);
                        if (!TextUtils.isEmpty(refreshedToken)) {
                            //Toast.makeText(getApplicationContext(), "Firebase Reg Id: " + regId, Toast.LENGTH_LONG).show();
                            Log.d("FCM", refreshedToken);
                            updateFcmToken(user_id,userkey,refreshedToken,loginResponse);
                        } else {
                            Toast.makeText(getApplicationContext(), "Firebase Reg Id is not received yet!", Toast.LENGTH_LONG).show();
                        }

                    }
                });


    }

    // update Fcm Token
    private void updateFcmToken(String id, String userkey, String refreshedToken, LoginResponse loginResponse) {

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateFcmTocken(id, userkey, refreshedToken);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    ResponseBody responseBody = response.body();
                    Log.d(TAG, "onResponse: "+"Success");

                    session.createLogin(loginResponse.getUserdata().get(0).getBuyerid(),
                            loginResponse.getUserdata().get(0).getFullname(),
                            loginResponse.getUserdata().get(0).getEmail(),
                            loginResponse.getUserdata().get(0).getMobile(),
                            loginResponse.getUserdata().get(0).getStatus(),
                            loginResponse.getUserdata().get(0).getUserkey(),refreshedToken);


                    Intent intent = new Intent(LoginActivity.this, StroeLocationActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    Toasty.success(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getApplicationContext(),"failed  token",Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                btnLogin.setEnabled(true);
            }
        });
    }



    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            moveTaskToBack(true);
            Process.killProcess(Process.myPid());
            System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toasty.normal(this, "click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);


    }
}
package com.pivotasoft.selltezhygiene.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.github.demono.adapter.InfinitePagerAdapter;
import com.pivotasoft.selltezhygiene.R;
import com.pivotasoft.selltezhygiene.Response.BannersResponse;

import java.util.List;
import java.util.Random;

public class AutoViewPager extends InfinitePagerAdapter {

    private List<BannersResponse.CouponsdataBean> data;
    private Context context;

    public AutoViewPager(List<BannersResponse.CouponsdataBean> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup container) {
        BannersResponse.CouponsdataBean homeBannersBean = data.get(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.banner_card, container, false);
        }
        // Lookup view for data population
        TextView tvDiscription = (TextView) convertView.findViewById(R.id.txtDescription);
        TextView tvCode = (TextView) convertView.findViewById(R.id.txtCode);
        TextView tvValid = (TextView) convertView.findViewById(R.id.txtValid);
        LinearLayout parentLayout = (LinearLayout) convertView.findViewById(R.id.linearParent);

        tvDiscription.setText(homeBannersBean.getDescription());
        tvCode.setText(homeBannersBean.getCouponcode());
        tvValid.setText("Valid upto : "+homeBannersBean.getTodate());

        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(position);

        parentLayout.setBackgroundColor(color);
        return convertView;
    }

    public int getRandomColor(){
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }
}

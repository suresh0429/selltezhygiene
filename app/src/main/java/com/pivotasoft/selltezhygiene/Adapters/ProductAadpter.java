package com.pivotasoft.selltezhygiene.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.pivotasoft.selltezhygiene.R;
import com.pivotasoft.selltezhygiene.ProductDetailsActivity;
import com.pivotasoft.selltezhygiene.Response.ProductResponse;

import java.util.List;

import static com.pivotasoft.selltezhygiene.Apis.RetrofitClient.IMAGE_PRODUCT_URL;
import static com.pivotasoft.selltezhygiene.Apis.RetrofitClient.PRODUCT_IMAGE_BASE_URL2;
import static com.pivotasoft.selltezhygiene.Storage.Utilities.capitalize;

public class ProductAadpter extends RecyclerView.Adapter<ProductAadpter.MyViewHolder>{
    private Context mContext;
    private List<ProductResponse.ProductsdataBean> homeList;
    private String storeName;
    private String storeFcmKey;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnail;
        public TextView txtPrice,txtName,txtDiscountPrice,txtDiscountTag,product_unit;
        //  public RatingBar ratingBar;
        public CardView linearLayout;
        public LinearLayout tagLayout,shadowImageView;


        public MyViewHolder(View view) {
            super(view);

            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            txtPrice = (TextView) view.findViewById(R.id.txtproductPrice);
            product_unit = (TextView) view.findViewById(R.id.product_unit);
            txtName = (TextView) view.findViewById(R.id.txtproductName);
            txtDiscountPrice = (TextView) view.findViewById(R.id.txtDiscountPrice);
            linearLayout=(CardView)view.findViewById(R.id.parentLayout);
            tagLayout = (LinearLayout)view.findViewById(R.id.tagLayout);
            shadowImageView = (LinearLayout)view.findViewById(R.id.shadowImageView);
            txtDiscountTag = (TextView)view.findViewById(R.id.txtDiscountTag);

            // ratingBar=(RatingBar)view.findViewById(R.id.ratingBar);


        }
    }

    public ProductAadpter(Context mContext, List<ProductResponse.ProductsdataBean> homekitchenList, String storeName, String storeFcmKey) {
        this.mContext = mContext;
        this.homeList = homekitchenList;
        this.storeName = storeName;
        this.storeFcmKey = storeFcmKey;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.products_item_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductResponse.ProductsdataBean home = homeList.get(position);
        setFadeAnimation(holder.itemView);

        if (home.getStatus().equalsIgnoreCase("2")){
            holder.shadowImageView.setVisibility(View.VISIBLE);
            holder.linearLayout.setClickable(false);
            holder.linearLayout.setEnabled(false);
        }else {
            holder.shadowImageView.setVisibility(View.GONE);
            holder.linearLayout.setClickable(true);
            holder.linearLayout.setEnabled(true);

        }

        // loading album cover using Glide library
        Glide.with(mContext).load(IMAGE_PRODUCT_URL+home.getProductpic()).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(holder.thumbnail);
        Log.d("ADAPTERIMASGE", "onBindViewHolder: "+PRODUCT_IMAGE_BASE_URL2+home.getProductpic());
        holder.txtName.setText(capitalize(home.getTitle()));
        holder.txtDiscountPrice.setText("\u20B9"+home.getFinalprice());

        holder.txtPrice.setText("\u20B9" + home.getUnitprice());
        holder.txtPrice.setPaintFlags(holder.txtPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtPrice.setTextColor(Color.RED);

        holder.product_unit.setText("" +home.getMeasureunits());


        //  holder.ratingBar.setRating(Float.parseFloat(home.getRating()));
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                intent.putExtra("productId",home.getProductid());
                intent.putExtra("productName",home.getTitle());
                intent.putExtra("price",home.getUnitprice());
                intent.putExtra("discount",home.getFinalprice());
                intent.putExtra("discription",home.getDescription());
                intent.putExtra("status",home.getStatus());
                intent.putExtra("weight",home.getMeasureunits());
                intent.putExtra("storeId",home.getStoreid());
                intent.putExtra("storeName",storeName);
                intent.putExtra("storeFcmKey",storeFcmKey);
                intent.putExtra("productImage",IMAGE_PRODUCT_URL+home.getProductpic());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);


            }});

      /*  //discount tag
        if (home.getDiscount().isEmpty()){
            holder.tagLayout.setVisibility(View.GONE);
        }else {
            holder.tagLayout.setVisibility(View.VISIBLE);
            holder.txtDiscountTag.setText(home.getDiscount()+"Off");

        }*/

    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


    public void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }
}

package com.pivotasoft.selltezhygiene.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.pivotasoft.selltezhygiene.HomeActivity;
import com.pivotasoft.selltezhygiene.R;
import com.pivotasoft.selltezhygiene.Response.StoreLocations;
import com.pivotasoft.selltezhygiene.Storage.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.pivotasoft.selltezhygiene.Storage.Utilities.capitalize;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.MyViewHolder> {



    private Context mContext;
    private List<StoreLocations.StoresdataBean> homeList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtStoreName)
        TextView txtStoreName;
        @BindView(R.id.txtAddress)
        TextView txtAddress;
        @BindView(R.id.txtDistance)
        TextView txtDistance;
        @BindView(R.id.rate_img)
        RatingBar rateImg;
        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.parentLayout)
        LinearLayout parentLayout;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public LocationAdapter(Context mContext, List<StoreLocations.StoresdataBean> homekitchenList) {
        this.mContext = mContext;
        this.homeList = homekitchenList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final StoreLocations.StoresdataBean home = homeList.get(position);


        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
        Date dtFrom;
        Date dtTo;
        try {
            dtFrom = sdf.parse(home.getStarttime());
            dtTo = sdf.parse(home.getEndtime());
            holder.txtTime.setText(sdfs.format(dtFrom) + " - " + sdfs.format(dtTo));
            // System.out.println("Time Display: " + sdfs.format(dt)); // <-- I got result here
        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.txtStoreName.setText(capitalize(home.getTitle()));
        holder.txtAddress.setText(home.getAddress());
        holder.txtDistance.setText(home.getSupportkm() + " km");

        if (home.getRatings() != null) {
            holder.rateImg.setRating(Float.parseFloat(home.getRatings()));
        } else {
            holder.rateImg.setRating(0);
        }


        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("storeId", home.getStoreid());
                intent.putExtra("storeName", home.getTitle());
                intent.putExtra("storeFcmKey", home.getFcmtoken());
                mContext.startActivity(intent);

                // update location preferences
                SharedPreferences preferences = mContext.getSharedPreferences(Utilities.PREFS_LOCATION, 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, true);
                editor.putString(Utilities.KEY_STORE_ID, home.getStoreid());
                editor.putString(Utilities.KEY_STORE_NAME, home.getTitle());
                editor.putString(Utilities.KEY_STORE_FCM, home.getFcmtoken());
                editor.apply();


            }

        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


}

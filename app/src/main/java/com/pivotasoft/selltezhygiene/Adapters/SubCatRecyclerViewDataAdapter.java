package com.pivotasoft.selltezhygiene.Adapters;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.pivotasoft.selltezhygiene.Model.HeaderSectionDataModel;
import com.pivotasoft.selltezhygiene.Model.SingleItemModel;
import com.pivotasoft.selltezhygiene.ProductListActivity;
import com.pivotasoft.selltezhygiene.R;

import java.util.ArrayList;

import static com.pivotasoft.selltezhygiene.Storage.Utilities.capitalize;

public class SubCatRecyclerViewDataAdapter extends RecyclerView.Adapter<SubCatRecyclerViewDataAdapter.ItemRowHolder> {

    private ArrayList<HeaderSectionDataModel> dataList;
    private ArrayList<SingleItemModel> singleItemList;
    private Context mContext;
    private String storeName;
    private String storeFcmKey;

    public SubCatRecyclerViewDataAdapter(Context context, ArrayList<HeaderSectionDataModel> dataList, ArrayList<SingleItemModel> singleItemList, String storeName, String storeFcmKey) {
        this.dataList = dataList;
        this.mContext = context;
        this.singleItemList = singleItemList;
        this.storeName = storeName;
        this.storeFcmKey = storeFcmKey;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, final int i) {

//        final SingleItemModel singleItemModel = singleItemList.get(i);

        final String sectionName = dataList.get(i).getHeaderTitle();

        ArrayList singleSectionItems = dataList.get(i).getAllItemsInSection();

        itemRowHolder.itemTitle.setText(capitalize(sectionName));

        ProductSectionListDataAdapter itemListDataAdapter = new ProductSectionListDataAdapter(mContext, singleSectionItems,storeName);

        itemRowHolder.recycler_view_list.setHasFixedSize(true);
        itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);


         itemRowHolder.recycler_view_list.setNestedScrollingEnabled(false);

        itemRowHolder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, ProductListActivity.class);
                intent.putExtra("catId", dataList.get(i).getCategoryId());
                intent.putExtra("title", dataList.get(i).getHeaderTitle());
                intent.putExtra("subcatId", dataList.get(i).getSubcategoryId());
                intent.putExtra("storeId", dataList.get(i).getStoreId());
                intent.putExtra("storeName", storeName);
                intent.putExtra("storeFcmKey", storeFcmKey);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);




            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle;

        protected RecyclerView recycler_view_list;

        protected Button btnMore;



        public ItemRowHolder(View view) {
            super(view);

            this.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
            this.recycler_view_list = (RecyclerView) view.findViewById(R.id.recycler_view_list);
            this.btnMore= (Button) view.findViewById(R.id.btnMore);


        }

    }

}
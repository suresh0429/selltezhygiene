package com.pivotasoft.selltezhygiene.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.github.thunder413.datetimeutils.DateTimeStyle;
import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.pivotasoft.selltezhygiene.Interface.Dilogueinterface;
import com.pivotasoft.selltezhygiene.Model.OrderIdItem;
import com.pivotasoft.selltezhygiene.OrdersListActivity;
import com.pivotasoft.selltezhygiene.R;

import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.pivotasoft.selltezhygiene.Storage.Utilities.capitalize;

public class OrderIdAdapter extends RecyclerView.Adapter<OrderIdAdapter.MyViewHolder> {



    private Context mContext;
    private List<OrderIdItem> myOrdersListBeans;
    private Dilogueinterface dilogueinterface;

    public OrderIdAdapter(Context mContext, List<OrderIdItem> myOrdersListBeans, Dilogueinterface dilogueinterface, String storeName, String storeFcmKey) {
        this.mContext = mContext;
        this.myOrdersListBeans = myOrdersListBeans;
        this.dilogueinterface = dilogueinterface;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_thumbnail)
        ImageView imageThumbnail;
        @BindView(R.id.txtStorename)
        TextView txtStorename;
        @BindView(R.id.txtInvoiceId)
        TextView txtInvoiceId;
        @BindView(R.id.txtpaymentTitle)
        TextView txtpaymentTitle;
        @BindView(R.id.txtPaymentStatus)
        TextView txtPaymentStatus;
        @BindView(R.id.txtOrderStatus)
        TextView txtOrderStatus;
        @BindView(R.id.txtDate)
        TextView txtDate;
        @BindView(R.id.txtFinalBill)
        TextView txtFinalBill;
        @BindView(R.id.txtCall)
        TextView txtCall;
        @BindView(R.id.txtIssues)
        TextView txtIssues;
        @BindView(R.id.txtRating)
        TextView txtRating;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.orderidlayout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final OrderIdItem orderListBean = myOrdersListBeans.get(position);

        setFadeAnimation(holder.itemView);

        String firstLetter = " ";
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color1 = generator.getRandomColor();

        if (orderListBean.getTitle() != null) {
            firstLetter = capitalize(orderListBean.getTitle()).substring(0, 1);
            holder.txtStorename.setText(capitalize(orderListBean.getTitle()));
        }
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40) // height in px
                .endConfig()
                .buildRoundRect(firstLetter, color1, 5);

        holder.imageThumbnail.setImageDrawable(drawable);
        holder.txtInvoiceId.setText(orderListBean.getInvoiceno());
        holder.txtPaymentStatus.setText(orderListBean.getPaymentmode());
        if (orderListBean.getOrderstatus().equalsIgnoreCase("Pending")){
            holder.txtOrderStatus.setBackgroundResource(R.drawable.rounded_button_orange);
            holder.txtCall.setVisibility(View.VISIBLE);
        }else if (orderListBean.getOrderstatus().equalsIgnoreCase("Accepted")){
            holder.txtOrderStatus.setBackgroundResource(R.drawable.rounded_button_blue);
            holder.txtCall.setVisibility(View.VISIBLE);
        }else if (orderListBean.getOrderstatus().equalsIgnoreCase("Rejected")){
            holder.txtCall.setVisibility(View.GONE);
            holder.txtIssues.setVisibility(View.GONE);
            holder.txtOrderStatus.setBackgroundResource(R.drawable.rounded_button_red);
        }else if (orderListBean.getOrderstatus().equalsIgnoreCase("Delivered")){
            holder.txtOrderStatus.setBackgroundResource(R.drawable.rounded_button_yellow);
            holder.txtCall.setVisibility(View.VISIBLE);
        }else {
            holder.txtOrderStatus.setBackgroundResource(R.drawable.rounded_button_green);
            holder.txtCall.setVisibility(View.GONE);
        }
        holder.txtOrderStatus.setText(orderListBean.getOrderstatus());
        holder.txtFinalBill.setText("\u20b9" + String.format("%.2f", Double.parseDouble(orderListBean.getFinalbill())));


        Log.d("TIMEZONE", "String To Date >> " + DateTimeUtils.formatDate(orderListBean.getBookingdatetime()));

        Date date = DateTimeUtils.formatDate(orderListBean.getBookingdatetime());
        String fullDate = DateTimeUtils.formatWithStyle(date, DateTimeStyle.MEDIUM); // June 13, 2017

        StringTokenizer stringTokenizer = new StringTokenizer(fullDate);
        String monthDate = stringTokenizer.nextToken(",");
        String year = stringTokenizer.nextToken(",");

        StringTokenizer monthdateToken = new StringTokenizer(monthDate);
        String mm = monthdateToken.nextToken(" ");
        String dd = monthdateToken.nextToken(" ");

        Log.e("TOKEN", "" + mm + dd);

        if (dd.equalsIgnoreCase("1") || dd.equalsIgnoreCase("21") || dd.equalsIgnoreCase("31")){
            holder.txtDate.setText(dd+"st "+mm+" "+year);
        }else if (dd.equalsIgnoreCase("2") || dd.equalsIgnoreCase("22")){
            holder.txtDate.setText(dd+"nd "+mm+" "+year);
        }else if (dd.equalsIgnoreCase("3") || dd.equalsIgnoreCase("23")){
            holder.txtDate.setText(dd+"rd "+mm+" "+year);
        }else {
            holder.txtDate.setText(dd+"th "+mm+" "+year);
        }



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String order_id = orderListBean.getOrderid();

                Intent intent = new Intent(mContext, OrdersListActivity.class);
                intent.putExtra("Order_ID", order_id);
                intent.putExtra("CartStatus", false);
                intent.putExtra("finalBill", orderListBean.getFinalbill());
                intent.putExtra("expectedTime", orderListBean.getExpectedtime());
                intent.putExtra("paymentMode", orderListBean.getPaymentmode());
                intent.putExtra("paymentStatus", orderListBean.getPaymentstatus());
                intent.putExtra("orderStatus", orderListBean.getOrderstatus());
                intent.putExtra("storeId", orderListBean.getStoreid());
                intent.putExtra("storeName", orderListBean.getTitle());
                intent.putExtra("storeFcmKey", orderListBean.getFcmtoken());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

        if (orderListBean.getOrderstatus().equalsIgnoreCase("Delivered")) {
            holder.txtRating.setVisibility(View.VISIBLE);
        } else {
            holder.txtRating.setVisibility(View.GONE);

        }

        holder.txtIssues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(mContext, IssuesListActivity.class);
                intent.putExtra("Order_ID", orderListBean.getOrderid());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);*/
                dilogueinterface.onDilogIssuesClick(orderListBean);
            }
        });

        // view Items
        holder.txtRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dilogueinterface.onDilogClick(orderListBean);

            }
        });

        holder.txtCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("tel:"+orderListBean.getMobile()));
                mContext.startActivity(intent);

            }
        });

    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return myOrdersListBeans.size();
    }

    public void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }
}

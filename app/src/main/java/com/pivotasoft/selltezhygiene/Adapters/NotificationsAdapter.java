package com.pivotasoft.selltezhygiene.Adapters;

import android.app.AlertDialog;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pivotasoft.selltezhygiene.NotificationActivity;
import com.pivotasoft.selltezhygiene.R;
import com.pivotasoft.selltezhygiene.Response.NotificationsResponse;
import com.pivotasoft.selltezhygiene.Storage.PrefManager;

import java.util.List;

import static com.pivotasoft.selltezhygiene.Storage.Utilities.capitalize;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.Holder> {
    Context context;
    List<NotificationsResponse.NotificationsDataBean> dataBeans;
    String read_status,send_id,userId,tokenValue;
    private PrefManager pref;
    AlertDialog alertDialog;

    public NotificationsAdapter(NotificationActivity notificationActivity, List<NotificationsResponse.NotificationsDataBean> dataBeans) {
        this.context=notificationActivity;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public NotificationsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_notification_list, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationsAdapter.Holder holder, final int i) {

        holder.notify_title_name.setText(capitalize("Selltez"));
        holder.notify_description.setText(dataBeans.get(i).getContent());
        holder.notify_dateandtime.setText(dataBeans.get(i).getMsgtime());


    }



    @Override
    public int getItemCount() {
        return dataBeans.size();
    }
    class Holder extends RecyclerView.ViewHolder{
        ImageView imag_notification;
        TextView notify_title_name,notify_description,notify_dateandtime;
        LinearLayout linear_layout_row;

        public Holder(@NonNull View itemView) {
            super(itemView);

            notify_dateandtime=itemView.findViewById(R.id.notify_dateandtime);
            notify_description=itemView.findViewById(R.id.notify_description);
            notify_title_name=itemView.findViewById(R.id.notify_title_name);

            imag_notification=itemView.findViewById(R.id.imag_notification);

            linear_layout_row=itemView.findViewById(R.id.linear_layout_row);

        }
    }
}

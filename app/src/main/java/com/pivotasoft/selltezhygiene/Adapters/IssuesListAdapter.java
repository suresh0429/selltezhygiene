package com.pivotasoft.selltezhygiene.Adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.pivotasoft.selltezhygiene.Response.IssuesListResponse;

import java.util.List;

import static com.pivotasoft.selltezhygiene.R.id;
import static com.pivotasoft.selltezhygiene.R.layout;
import static com.pivotasoft.selltezhygiene.Storage.Utilities.capitalize;

public class IssuesListAdapter extends RecyclerView.Adapter<IssuesListAdapter.MyViewHolder> {
    private Context mContext;
    List<IssuesListResponse.IssuelistdataBean> productsBeanList;
    public IssuesListAdapter(Context context, List<IssuesListResponse.IssuelistdataBean> productsBeanList) {
        this.mContext=context;
        this.productsBeanList=productsBeanList;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView txtDescription,txtAssignedDate,txtStoreName,txtCustomerDescription,txtResolvedDate,txtInvoice;
        ImageView imageThumbnail;
        public MyViewHolder(View itemView) {
            super(itemView);

            imageThumbnail = (ImageView)itemView.findViewById(id.image_thumbnail);
            txtStoreName = (TextView)itemView.findViewById(id.txtStoreName);
            txtDescription = (TextView)itemView.findViewById(id.txtDescription);
            txtAssignedDate = (TextView)itemView.findViewById(id.txtassignedon);
            txtCustomerDescription = (TextView)itemView.findViewById(id.txtDescriptionCustomer);
            txtResolvedDate = (TextView)itemView.findViewById(id.txtresolvedon);
            txtInvoice = (TextView)itemView.findViewById(id.txtInvoiceId);

        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(layout.issue_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        String firstLetter = " ";
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color1 = generator.getRandomColor();

        if (productsBeanList.get(position).getTitle() != null) {
            firstLetter = capitalize(productsBeanList.get(position).getTitle()).substring(0, 1);
            holder.txtStoreName.setText(productsBeanList.get(position).getTitle());
        }
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40) // height in px
                .endConfig()
                .buildRoundRect(firstLetter, color1, 5);

        holder.imageThumbnail.setImageDrawable(drawable);


        holder.txtDescription.setText(productsBeanList.get(position).getDescription());
        holder.txtAssignedDate.setText("Posted On : "+productsBeanList.get(position).getAssignedon());
        holder.txtCustomerDescription.setText(productsBeanList.get(position).getResolvedescription());

        if (productsBeanList.get(position).getResolvedon() != null){
            holder.txtResolvedDate.setText("Resolved On : "+productsBeanList.get(position).getResolvedon());
        }else {
            holder.txtResolvedDate.setText("Resolved On : "+"----------");
        }

        holder.txtInvoice.setText("Invoice Id : "+productsBeanList.get(position).getInvoiceno());


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return productsBeanList.size();
    }






}

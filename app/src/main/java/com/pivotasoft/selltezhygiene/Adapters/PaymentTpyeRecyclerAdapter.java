package com.pivotasoft.selltezhygiene.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pivotasoft.selltezhygiene.CheckoutActivity;
import com.pivotasoft.selltezhygiene.Interface.PaymentTypeInterface;
import com.pivotasoft.selltezhygiene.R;
import com.pivotasoft.selltezhygiene.Response.CheckoutResponse;

import java.util.List;

import static com.pivotasoft.selltezhygiene.Apis.RetrofitClient.IMAGE_PRODUCT_URL;
import static com.pivotasoft.selltezhygiene.Storage.Utilities.capitalize;

public class PaymentTpyeRecyclerAdapter extends RecyclerView.Adapter<PaymentTpyeRecyclerAdapter.Holder> {
    Context context;
    public int lastSelectedPosition = -1;
    public static String cash;
    List<CheckoutResponse.DataBean.PaymentGatewayBean> paymentGatewayBeans;
    private PaymentTypeInterface paymentTypeInterface;

    public PaymentTpyeRecyclerAdapter(CheckoutActivity checkoutActivity, List<CheckoutResponse.DataBean.PaymentGatewayBean> paymentGatewayBeans,PaymentTypeInterface paymentTypeInterface) {
        this.context=checkoutActivity;
        this.paymentGatewayBeans=paymentGatewayBeans;
        this.paymentTypeInterface=paymentTypeInterface;
    }

    @NonNull
    @Override
    public PaymentTpyeRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_paymentmethods, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PaymentTpyeRecyclerAdapter.Holder holder, final int i) {



        holder.offer_name.setText(capitalize(paymentGatewayBeans.get(i).getName()));
        holder.offer_select.setChecked(lastSelectedPosition == i);

        Glide.with(context).load(IMAGE_PRODUCT_URL +paymentGatewayBeans.get(i).getLogo()).into(holder.p_image);

        Log.d("IMAGE", "onBindViewHolder: "+ IMAGE_PRODUCT_URL +paymentGatewayBeans.get(i).getLogo());
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastSelectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();

                paymentTypeInterface.onItemClick(paymentGatewayBeans,i);
                /*Log.d("POSITION", "onClick: "+paymentGatewayBeans.);*/


                // cash = paymentGatewayBeans.get(lastSelectedPosition).getId();
            }
        };
        holder.itemView.setOnClickListener(clickListener);
        holder.offer_select.setOnClickListener(clickListener);

    }

    @Override
    public int getItemCount() {
        return paymentGatewayBeans.size();
    }

    class Holder extends RecyclerView.ViewHolder{

        TextView offer_name;
        RadioButton offer_select;
        ImageView p_image;

        public Holder(@NonNull final View itemView) {
            super(itemView);

            offer_select = itemView.findViewById(R.id.offer_select);
            offer_name = itemView.findViewById(R.id.offer_name);
            p_image = itemView.findViewById(R.id.p_image);

           /* View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();

                    paymentTypeInterface.onItemClick(itemView.getId());
                    Log.d("POSITION", "onClick: "+itemView.getId());


                   // cash = paymentGatewayBeans.get(lastSelectedPosition).getId();
                }
            };
            itemView.setOnClickListener(clickListener);
            offer_select.setOnClickListener(clickListener);*/
        }
    }
}

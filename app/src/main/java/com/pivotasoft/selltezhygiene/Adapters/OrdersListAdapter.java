package com.pivotasoft.selltezhygiene.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.pivotasoft.selltezhygiene.OrdersListActivity;
import com.pivotasoft.selltezhygiene.Response.OrderListResponse;
import com.pivotasoft.selltezhygiene.R;

import java.util.List;

import static com.pivotasoft.selltezhygiene.Apis.RetrofitClient.IMAGE_PRODUCT_URL;
import static com.pivotasoft.selltezhygiene.R.*;
import static com.pivotasoft.selltezhygiene.Storage.Utilities.capitalize;

public class OrdersListAdapter extends RecyclerView.Adapter<OrdersListAdapter.MyViewHolder> {
    private Context mContext;
    List<OrderListResponse.ItemdataBean> productsBeanList;
    public OrdersListAdapter(OrdersListActivity ordersListActivity, List<OrderListResponse.ItemdataBean> productsBeanList) {
        this.mContext=ordersListActivity;
        this.productsBeanList=productsBeanList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView imgOrder;
        TextView txtOrderName,txtOrderID,txtOrderPrice,txtStatus,txtReview,txtQty;
        LinearLayout reviewLayout;


        public MyViewHolder(View itemView) {
            super(itemView);

            imgOrder = (ImageView)itemView.findViewById(id.imgOrder);
            txtOrderName = (TextView)itemView.findViewById(id.txtOrderName);
            txtOrderID = (TextView)itemView.findViewById(id.txtOrderID);
            txtOrderPrice = (TextView)itemView.findViewById(id.txtOrderPrice);
            txtQty=(TextView)itemView.findViewById(R.id.txtQty);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.myorderlistmodel1, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        Glide.with(mContext).load(IMAGE_PRODUCT_URL+productsBeanList.get(position).getProductpic()).error(R.drawable.default_loading).into(holder.imgOrder);

        holder.txtOrderName.setText(capitalize(productsBeanList.get(position).getTitle()));
//        holder.txtOrderID.setText(productsBeanList.get(position).getProductId());
        holder.txtQty.setText("Product Quantity : "+productsBeanList.get(position).getQuantity());
        holder.txtOrderPrice.setText(mContext.getResources().getString(R.string.Rs)+" "+ productsBeanList.get(position).getFinalprice());


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return productsBeanList.size();
    }



//    private void prepareReviewData(final View view, final String module, final String userid, final String productId, final String username, final String email, final String rating, final String message){
//
//
//        Call<ReviewResponse> call = RetrofitClient.getInstance().getApi().reviewOrder( module, userid, productId, username, email, rating, message);
//
//        call.enqueue(new Callback<ReviewResponse>() {
//            @Override
//            public void onResponse(Call<ReviewResponse> call, retrofit2.Response<ReviewResponse> response) {
//
//                ReviewResponse reviewResponse = response.body();
//
//                if (response.isSuccessful()) {
//
//                    if ((reviewResponse != null ? reviewResponse.getStatus() : 0) ==1){
//
//                        Snackbar.make(view,reviewResponse.getMessage(),Snackbar.LENGTH_SHORT).show();
//
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                dialog.dismiss();
//
//                            }
//                        }, 2000);
//
//                    }
//
//
//
//
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<ReviewResponse> call, Throwable t) {
//
//
//            }
//        });
//    }


}

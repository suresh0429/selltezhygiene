package com.pivotasoft.selltezhygiene;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.demono.AutoScrollViewPager;
import com.pivotasoft.selltezhygiene.Adapters.AutoViewPager;
import com.pivotasoft.selltezhygiene.Adapters.HomeAdapter;
import com.pivotasoft.selltezhygiene.Apis.RetrofitClient;
import com.pivotasoft.selltezhygiene.Helper.Converter;
import com.pivotasoft.selltezhygiene.Reciever.ConnectivityReceiver;
import com.pivotasoft.selltezhygiene.Response.BannersResponse;
import com.pivotasoft.selltezhygiene.Response.CategoriesResponse;
import com.pivotasoft.selltezhygiene.Singleton.AppController;
import com.pivotasoft.selltezhygiene.Storage.PrefManager;
import com.pivotasoft.selltezhygiene.Storage.Utilities;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.pivotasoft.selltezhygiene.Apis.RetrofitClient.ABOUT_US_URL;
import static com.pivotasoft.selltezhygiene.Apis.RetrofitClient.FAQ_URL;
import static com.pivotasoft.selltezhygiene.Apis.RetrofitClient.PRIVACY_POLICY_URL;
import static com.pivotasoft.selltezhygiene.Apis.RetrofitClient.RETURN_POLICY_URL;
import static com.pivotasoft.selltezhygiene.Apis.RetrofitClient.TERMS_CONDITIONS_URL;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    AlertDialog.Builder builder;
    boolean doubleBackToExitPressedOnce = false;
    ConnectivityReceiver connectivityReceiver;
    int color = Color.RED;
    AppController appController;
    @BindView(R.id.homeRecycler)
    RecyclerView homeRecycler;
    @BindView(R.id.popularRecycler)
    RecyclerView popularRecycler;
    @BindView(R.id.viewPager)
    AutoScrollViewPager viewPager;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.emptyBanner)
    LinearLayout emptyBanner;


    private PrefManager pref;
    Snackbar snackbar;
    int cartindex;
    String userId, imagePic, deviceId, tokenValue, loc_area, checnkVeriosn, android_version, storeId, storeName,keyFcm,storeFcmKey;
    ImageView nav_Image;
    String title = "";
    TextView nav_username, nav_useremail, txtShop;
    //    private FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    private HashMap<String, Object> firebaseDefaultMap;
    private static final String TAG = "HomeActivity";
    private static final String VERSION_CODE_KEY = "app_latest_version";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.selltezlogo);
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //LandScape mode
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        appController = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());


        // Method to manually check connection status
        boolean isConnected = appController.isConnection();
        //showSnack(isConnected);

        if (getIntent() != null) {
            storeId = getIntent().getStringExtra("storeId");
            storeName = getIntent().getStringExtra("storeName");
            storeFcmKey = getIntent().getStringExtra("storeFcmKey");
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView = navigationView.getHeaderView(0);
        nav_username = (TextView) hView.findViewById(R.id.userName);
        nav_useremail = (TextView) hView.findViewById(R.id.userEmail);
        nav_Image = (ImageView) hView.findViewById(R.id.imageView);


        // search Layout
        TextView searchLayout = (TextView) findViewById(R.id.searchLayout);
        searchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, SearchActivity.class);
                intent.putExtra("storeId",storeId);
                intent.putExtra("storeName",storeName);
                intent.putExtra("storeFcmKey",storeFcmKey);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        homeRecycler.setLayoutManager(mLayoutManager);
        homeRecycler.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        homeRecycler.setItemAnimator(new DefaultItemAnimator());

        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        popularRecycler.setLayoutManager(mLayoutManager1);
        popularRecycler.setItemAnimator(new DefaultItemAnimator());


        if (isConnected) {

            userData();


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("storeId",storeId);
                    intent.putExtra("storeName",storeName);
                    intent.putExtra("storeFcmKey",storeFcmKey);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }


    }


    // user information
    private void userData() {
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        String username = profile.get("name");
        String email = profile.get("email");
        imagePic = profile.get("profilepic");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");
        keyFcm = profile.get("keyFcm");

        nav_useremail.setText(email);
        nav_username.setText(username);

        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {
            // loading album cover using Glide library
            Glide.with(getApplicationContext()).load(imagePic).into(nav_Image);
        } else {

            nav_Image.setImageResource(R.drawable.ic_user);
        }

        progressBar.setVisibility(View.VISIBLE);

        prepareHomeData(tokenValue);
        prepareHomeBannersData(storeId);


    }





    // cart count
    private void cartCount() {

        appController.cartCount(userId, tokenValue);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindex", "" + cartindex);
        invalidateOptionsMenu();


    }


    // home banners
    public void prepareHomeBannersData(String storeId) {

        Call<BannersResponse> call = RetrofitClient.getInstance().getApi().storeCoupens(tokenValue, storeId);
        call.enqueue(new Callback<BannersResponse>() {
            @Override
            public void onResponse(Call<BannersResponse> call, Response<BannersResponse> response) {
                BannersResponse bannersResponse = response.body();
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    assert response.body() != null;
                    final List<BannersResponse.CouponsdataBean> homeBannersBeanList = response.body().getCouponsdata();

                    if (homeBannersBeanList.size() != 0) {
                        AutoViewPager mAdapter = new AutoViewPager(homeBannersBeanList, HomeActivity.this);
                        viewPager.setAdapter(mAdapter);
                        // optional start auto scroll
                        viewPager.startAutoScroll();
                        emptyBanner.setVisibility(View.GONE);
                    } else {
                        emptyBanner.setVisibility(View.VISIBLE);
                        viewPager.setVisibility(View.GONE);

                    }


                }


            }

            @Override
            public void onFailure(Call<BannersResponse> call, Throwable t) {
                Snackbar.make(homeRecycler, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();

                progressBar.setVisibility(View.GONE);
            }
        });

    }

    // home modules
    private void prepareHomeData(String tokenValue) {


        Call<CategoriesResponse> call = RetrofitClient.getInstance().getApi().categories(tokenValue);
        call.enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                CategoriesResponse addressResponse = response.body();
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    // modules
                    final List<CategoriesResponse.CategoriesdataBean> modulesBeanList = response.body().getCategoriesdata();


                    final HomeAdapter adapter = new HomeAdapter(getApplicationContext(), modulesBeanList, storeId,storeName,storeFcmKey);
                    homeRecycler.setAdapter(adapter);

                } else {
                    Toasty.success(getApplicationContext(), "No data Found", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                Snackbar.make(homeRecycler, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });

    }


    private void Logout() {
        SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
           /* editor.putString(Utilities.KEY_STORE_ID,home.getStoreid());
            editor.putString(Utilities.KEY_STORE_NAME,home.getTitle());*/
        editor.apply();

        pref.clearSession();
        //Logout
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();

        userData();
        cartCount();
//        locationShowFirsttime(currentLocation.getLatitude(), currentLocation.getLongitude());

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        userData();
        cartCount();
        // locationShowFirsttime(currentLocation.getLatitude(), currentLocation.getLongitude());

    }

    @Override
    protected void onStart() {
        super.onStart();

        userData();
        cartCount();
//        locationShowFirsttime(currentLocation.getLatitude(), currentLocation.getLongitude());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds countries to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        final MenuItem alertMenuItem = menu.findItem(R.id.action_location);
        RelativeLayout rootView = (RelativeLayout) alertMenuItem.getActionView();
        txtShop = (TextView) rootView.findViewById(R.id.txt_area);
        txtShop.setText(storeName);

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(alertMenuItem);
            }
        });


        final MenuItem menuItem = menu.findItem(R.id.action_cart);

        menuItem.setIcon(Converter.convertLayoutToImage(HomeActivity.this, cartindex, R.drawable.ic_actionbar_bag));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {

            Intent intent = new Intent(getApplicationContext(), CartActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("storeId",storeId);
            intent.putExtra("storeName",storeName);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            return true;
        } else if (id == R.id.action_location) {

            if(cartindex != 0){
                ClearCart();
            }
            else {
                Intent intent = new Intent(getApplicationContext(), StroeLocationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
                                     /*editor.putString(Utilities.KEY_STORE_ID,home.getStoreid());
                                    editor.putString(Utilities.KEY_STORE_NAME,home.getTitle());*/
                editor.apply();
            }

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_account) {
            //My Account
            Intent intent = new Intent(HomeActivity.this, MyAccountActivity.class);
            intent.putExtra("Checkout", false);
            intent.putExtra("storeId",storeId);
            intent.putExtra("storeName",storeName);
            intent.putExtra("storeFcmKey",storeFcmKey);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.My_orders) {
            //My orders
            Intent intent = new Intent(HomeActivity.this, OrderIdActvity.class);
            intent.putExtra("Checkout", false);
            intent.putExtra("storeId",storeId);
            intent.putExtra("storeName",storeName);
            intent.putExtra("storeFcmKey",storeFcmKey);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.action_notification) {

            Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.nav_logout) {

            Logout();

        } else if (id == R.id.nav_issues) {

            Intent intent = new Intent(getApplicationContext(), IssuesListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.nav_share) {

            String sAux = "Install Selltez application click below link \n" + "https://play.google.com/store/apps/details?id=com.pivotasoft.selltez";
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, sAux);
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);

        }   /* else if (id == R.id.nav_support) {


            Intent intent = new Intent(getApplicationContext(), SupportUsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }*/ else if (id == R.id.nav_privacy) {

            title = "Privacy & Policy";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", PRIVACY_POLICY_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_terms) {

            title = "Terms & Conditions";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", TERMS_CONDITIONS_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_return_policy) {

            title = "Return Policy";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", RETURN_POLICY_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_faq) {

            title = "FAQ's";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", FAQ_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_aboutus) {

            title = "About Us";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", ABOUT_US_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toasty.error(this, "click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);


            }
        }
    }


    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void ClearCart(){

        String message = "Your cart contains items from "+storeName+" .Do you want discard the selection and add items from other store.";
        //Setting message manually and performing action on button click
        builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        // claer cart from server
                        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().clearCart(tokenValue,userId);
                        call.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                progressBar.setVisibility(View.GONE);
                                if (response.isSuccessful()) {

                                    Intent intent = new Intent(getApplicationContext(), StroeLocationActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                                    SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
                                     /*editor.putString(Utilities.KEY_STORE_ID,home.getStoreid());
                                    editor.putString(Utilities.KEY_STORE_NAME,home.getTitle());*/
                                     editor.apply();

                                    cartCount();

                                } else {
                                    Toasty.normal(getApplicationContext(), "No data Found", Toast.LENGTH_SHORT).show();
                                }


                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                               // Snackbar.make(homeRecycler, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);

                            }
                        });


                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();

                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Replace cart item?");
        alert.show();
    }


}
